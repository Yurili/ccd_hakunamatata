######START###############
Projekt neu anlegen:
1. neues Repo mit Url auf PC erstellen
2. dieses Repo als Workspace auswählen
3. Projekt importieren
4. Build Path anpassen ( siehe nächste Kommentar)

Ahoi Folks,
###########################
#    Issue-Solving!!!!    #
###########################

>>>> org.jdesktop wird nicht gefunden
-----------------------------------------

Lösung:
1. Rechtsklick auf Projekt CCD_HakunaMatata -> Properties -> Java Build Path
2. Auf Reiter Libraries wechseln -> Add External JARs
3. In den Repo Ordner gehen und beide JARs in \lib auswählen

