var namespacejchess_1_1pieces =
[
    [ "Bishop", "classjchess_1_1pieces_1_1_bishop.html", "classjchess_1_1pieces_1_1_bishop" ],
    [ "King", "classjchess_1_1pieces_1_1_king.html", "classjchess_1_1pieces_1_1_king" ],
    [ "Knight", "classjchess_1_1pieces_1_1_knight.html", "classjchess_1_1pieces_1_1_knight" ],
    [ "Pawn", "classjchess_1_1pieces_1_1_pawn.html", "classjchess_1_1pieces_1_1_pawn" ],
    [ "Piece", "classjchess_1_1pieces_1_1_piece.html", "classjchess_1_1pieces_1_1_piece" ],
    [ "Queen", "classjchess_1_1pieces_1_1_queen.html", "classjchess_1_1pieces_1_1_queen" ],
    [ "Rook", "classjchess_1_1pieces_1_1_rook.html", "classjchess_1_1pieces_1_1_rook" ]
];