var classjchess_1_1game_logic_1_1_geometric_operations =
[
    [ "GeometricOperations", "classjchess_1_1game_logic_1_1_geometric_operations.html#a70d5c2e9a94decf7c5114ca9a9554d69", null ],
    [ "getInstance", "classjchess_1_1game_logic_1_1_geometric_operations.html#a84f3c5f428e3bbc936f59769df09d833", null ],
    [ "getPolygonShapeByPointAndVertices", "classjchess_1_1game_logic_1_1_geometric_operations.html#a5efacd2239be2049d973c4348bae32af", null ],
    [ "getRadiansByAngle", "classjchess_1_1game_logic_1_1_geometric_operations.html#a620eb40f4c204556fce976bcc6c387fa", null ],
    [ "scaleImage", "classjchess_1_1game_logic_1_1_geometric_operations.html#a23009e92f7400ef2de78d3861aa25a81", null ],
    [ "instance", "classjchess_1_1game_logic_1_1_geometric_operations.html#a9b3eab8d35254834ff63e1836d8458fb", null ]
];