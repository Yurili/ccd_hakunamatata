var classjchess_1_1_player =
[
    [ "STATUS", "enumjchess_1_1_player_1_1_s_t_a_t_u_s.html", "enumjchess_1_1_player_1_1_s_t_a_t_u_s" ],
    [ "Player", "classjchess_1_1_player.html#a5784e98a0365eeba7fada2efe4305214", null ],
    [ "getId", "classjchess_1_1_player.html#a1381ef88dce081c530a791beced6f3fe", null ],
    [ "getName", "classjchess_1_1_player.html#a83cab721f993c64481ec93e6bc6d2b99", null ],
    [ "getStatus", "classjchess_1_1_player.html#a64e8ee480a7ccf7dce37bdbe0e48afbd", null ],
    [ "setName", "classjchess_1_1_player.html#af1d71edefc3de81d93621b79af2dd5ca", null ],
    [ "setStatus", "classjchess_1_1_player.html#a13aef06c34313b580034694c53850918", null ],
    [ "id", "classjchess_1_1_player.html#a50b161fd16a925d03408a248c1bd913d", null ],
    [ "name", "classjchess_1_1_player.html#a121c6373ca91deebb967646734e55765", null ],
    [ "numberOfPlayer", "classjchess_1_1_player.html#acb87aef342a045697c23dd31fedae3b4", null ],
    [ "status", "classjchess_1_1_player.html#ae38c91a7953977d648ac3f464d693d12", null ]
];