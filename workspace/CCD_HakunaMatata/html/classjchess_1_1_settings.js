var classjchess_1_1_settings =
[
    [ "Settings", "classjchess_1_1_settings.html#ad8d0543a847bbd77c4e68b6165499fdc", null ],
    [ "getActivePlayer", "classjchess_1_1_settings.html#a7a13d6a034033c8b158bee36d832d6ab", null ],
    [ "getInstance", "classjchess_1_1_settings.html#a7cf104552d03a4f2f8f0490ed4b85e84", null ],
    [ "getListPlayer", "classjchess_1_1_settings.html#ab61c6873d254b55d823d4a7ce55ba747", null ],
    [ "getPlayerByID", "classjchess_1_1_settings.html#a583a7b5058d160fa42d169f86f2bcb17", null ],
    [ "lang", "classjchess_1_1_settings.html#a8416ef95f5e4c41aecd6ceba6281388e", null ],
    [ "setActivePlayer", "classjchess_1_1_settings.html#a9fea9cec0e5b3ea5ee7adc99be32c67f", null ],
    [ "switchActivePlayer", "classjchess_1_1_settings.html#a867529737cdc3be8c96f0d9158be37cb", null ],
    [ "activePlayer", "classjchess_1_1_settings.html#af83206b4a8c08827639f8c6fb5bbfe08", null ],
    [ "listPlayer", "classjchess_1_1_settings.html#ad3d948a1ceb53469f84cf4c3bc838a76", null ],
    [ "loc", "classjchess_1_1_settings.html#a50c2337c2d8bd2ba0e57de6c1d00939c", null ],
    [ "settings", "classjchess_1_1_settings.html#ab4dd315a21dbb01cc25cae7bca4bf05c", null ]
];