var namespacejchess_1_1test =
[
    [ "TestChessVector", "classjchess_1_1test_1_1_test_chess_vector.html", "classjchess_1_1test_1_1_test_chess_vector" ],
    [ "TestGamePanel", "classjchess_1_1test_1_1_test_game_panel.html", "classjchess_1_1test_1_1_test_game_panel" ],
    [ "TestGeometricOperations", "classjchess_1_1test_1_1_test_geometric_operations.html", "classjchess_1_1test_1_1_test_geometric_operations" ],
    [ "TestMovement", "classjchess_1_1test_1_1_test_movement.html", "classjchess_1_1test_1_1_test_movement" ],
    [ "TestSettings", "classjchess_1_1test_1_1_test_settings.html", "classjchess_1_1test_1_1_test_settings" ]
];