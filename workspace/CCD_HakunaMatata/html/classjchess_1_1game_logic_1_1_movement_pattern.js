var classjchess_1_1game_logic_1_1_movement_pattern =
[
    [ "MovementPattern", "classjchess_1_1game_logic_1_1_movement_pattern.html#a54505d13dc0410b479457a610aea2c19", null ],
    [ "addMoveOption", "classjchess_1_1game_logic_1_1_movement_pattern.html#a5d69e0955025da24373b2759267e192d", null ],
    [ "addMoveOption", "classjchess_1_1game_logic_1_1_movement_pattern.html#a73935686574f7aa37b4223a1bd4fda8a", null ],
    [ "addStrikeOption", "classjchess_1_1game_logic_1_1_movement_pattern.html#aa7b4242157e73a7d161c7e43ec44259a", null ],
    [ "addStrikeOption", "classjchess_1_1game_logic_1_1_movement_pattern.html#a685282f4750f7f33e20ebbff3052c7ad", null ],
    [ "getMovingOptions", "classjchess_1_1game_logic_1_1_movement_pattern.html#a8b324d13e3e7e185d7f3862c9d23d321", null ],
    [ "getStrikingOptions", "classjchess_1_1game_logic_1_1_movement_pattern.html#a57fc13b9476194ceaa521fd193739b9c", null ],
    [ "isIterative", "classjchess_1_1game_logic_1_1_movement_pattern.html#a99cfeda35b48b88d4741e6d67f36d05b", null ],
    [ "iterative", "classjchess_1_1game_logic_1_1_movement_pattern.html#affd651c47c3295bb0ce8c075ddad8886", null ],
    [ "moving", "classjchess_1_1game_logic_1_1_movement_pattern.html#ac1fe3f36fc5253a61e012934909449f0", null ],
    [ "onlystriking", "classjchess_1_1game_logic_1_1_movement_pattern.html#a71270e885ae196dece28475c29ffe3a0", null ]
];