var namespacejchess_1_1game_logic =
[
    [ "Chessboard", "classjchess_1_1game_logic_1_1_chessboard.html", "classjchess_1_1game_logic_1_1_chessboard" ],
    [ "ChessVector", "classjchess_1_1game_logic_1_1_chess_vector.html", "classjchess_1_1game_logic_1_1_chess_vector" ],
    [ "GeometricOperations", "classjchess_1_1game_logic_1_1_geometric_operations.html", "classjchess_1_1game_logic_1_1_geometric_operations" ],
    [ "Hexagon", "classjchess_1_1game_logic_1_1_hexagon.html", "classjchess_1_1game_logic_1_1_hexagon" ],
    [ "Movement", "classjchess_1_1game_logic_1_1_movement.html", "classjchess_1_1game_logic_1_1_movement" ],
    [ "MovementPattern", "classjchess_1_1game_logic_1_1_movement_pattern.html", "classjchess_1_1game_logic_1_1_movement_pattern" ]
];