var classjchess_1_1pieces_1_1_piece =
[
    [ "Piece", "classjchess_1_1pieces_1_1_piece.html#ac2785a3ca18932a0b4e8c372969a670a", null ],
    [ "getAllMoves", "classjchess_1_1pieces_1_1_piece.html#afc60f91d21e98ae7e8caeaacff8c4a86", null ],
    [ "getOrgImage", "classjchess_1_1pieces_1_1_piece.html#a46469ba29059f12f2e0dfc0bf1741f1d", null ],
    [ "getPlayer", "classjchess_1_1pieces_1_1_piece.html#a32e05ca3ee518b8fb88c4466684f9915", null ],
    [ "setAllMoves", "classjchess_1_1pieces_1_1_piece.html#ae984577ffe3c865a0f2c4c61bba8e086", null ],
    [ "allMoves", "classjchess_1_1pieces_1_1_piece.html#a9124568a457e761e27ef1cbc8c095e78", null ],
    [ "orgImage", "classjchess_1_1pieces_1_1_piece.html#a1dac1c9d13083d1ab7c1be10a952c5fb", null ],
    [ "player", "classjchess_1_1pieces_1_1_piece.html#ab4dcfe6a02e80717f31655ee6d95649b", null ]
];