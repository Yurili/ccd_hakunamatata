var classjchess_1_1test_1_1_test_game_panel =
[
    [ "setUpBeforeClass", "classjchess_1_1test_1_1_test_game_panel.html#a944435d85392db51a870f8634cf22040", null ],
    [ "testComputeGeometricProperties", "classjchess_1_1test_1_1_test_game_panel.html#a942dc8606c2163ac67fa4e96ac796ef5", null ],
    [ "testGamePanelConstructor", "classjchess_1_1test_1_1_test_game_panel.html#a06a35420e56fd8be97a812292c5bc651", null ],
    [ "testChessboard", "classjchess_1_1test_1_1_test_game_panel.html#af5616c0ba5bf54bce6ebb087dee956ff", null ],
    [ "testDimension", "classjchess_1_1test_1_1_test_game_panel.html#a37f237cf6951350ba74cade8d76fa665", null ],
    [ "testGamePanel", "classjchess_1_1test_1_1_test_game_panel.html#aca6fab239426ae5d888087fb8abbdd6f", null ]
];