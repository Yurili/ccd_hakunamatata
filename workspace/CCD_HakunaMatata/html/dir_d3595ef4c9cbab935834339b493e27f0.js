var dir_d3595ef4c9cbab935834339b493e27f0 =
[
    [ "TestChessVector.java", "_test_chess_vector_8java.html", [
      [ "TestChessVector", "classjchess_1_1test_1_1_test_chess_vector.html", "classjchess_1_1test_1_1_test_chess_vector" ]
    ] ],
    [ "TestGamePanel.java", "_test_game_panel_8java.html", [
      [ "TestGamePanel", "classjchess_1_1test_1_1_test_game_panel.html", "classjchess_1_1test_1_1_test_game_panel" ]
    ] ],
    [ "TestGeometricOperations.java", "_test_geometric_operations_8java.html", [
      [ "TestGeometricOperations", "classjchess_1_1test_1_1_test_geometric_operations.html", "classjchess_1_1test_1_1_test_geometric_operations" ]
    ] ],
    [ "TestMovement.java", "_test_movement_8java.html", [
      [ "TestMovement", "classjchess_1_1test_1_1_test_movement.html", "classjchess_1_1test_1_1_test_movement" ]
    ] ],
    [ "TestSettings.java", "_test_settings_8java.html", [
      [ "TestSettings", "classjchess_1_1test_1_1_test_settings.html", "classjchess_1_1test_1_1_test_settings" ]
    ] ]
];