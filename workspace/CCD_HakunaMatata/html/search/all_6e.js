var searchData=
[
  ['name',['name',['../classjchess_1_1_player.html#a121c6373ca91deebb967646734e55765',1,'jchess::Player']]],
  ['newgame',['newGame',['../classjchess_1_1_j_chess_view.html#ab915a14d8c38ea10112ac0946f1fff24',1,'jchess.JChessView.newGame()'],['../classjchess_1_1_game_panel.html#a536cfc591b498a393f3fb4de389b3209',1,'jchess.GamePanel.newGame()']]],
  ['newgameframe',['newGameFrame',['../classjchess_1_1_j_chess_view.html#a50e81977796444f79f4aae6ca7e95218',1,'jchess::JChessView']]],
  ['newgameitem',['newGameItem',['../classjchess_1_1_j_chess_view.html#a6544f1fa8caaa62ed3a4d12b857283d4',1,'jchess::JChessView']]],
  ['newgamewindow',['NewGameWindow',['../classjchess_1_1_new_game_window.html#a693bd6b4b346e219bd12f09b0b67e061',1,'jchess::NewGameWindow']]],
  ['newgamewindow',['NewGameWindow',['../classjchess_1_1_new_game_window.html',1,'jchess']]],
  ['newgamewindow_2ejava',['NewGameWindow.java',['../_new_game_window_8java.html',1,'']]],
  ['numberofplayer',['numberOfPlayer',['../classjchess_1_1_player.html#acb87aef342a045697c23dd31fedae3b4',1,'jchess::Player']]]
];
