var searchData=
[
  ['secondname',['secondName',['../classjchess_1_1_draw_local_settings.html#a30d7c896a73fbbdba9d64797e66c5ede',1,'jchess::DrawLocalSettings']]],
  ['secondnamecolor',['secondNameColor',['../classjchess_1_1_draw_local_settings.html#aa0face1e448115f394e6389383b5cc58',1,'jchess::DrawLocalSettings']]],
  ['secondnamelab',['secondNameLab',['../classjchess_1_1_draw_local_settings.html#a9d19d398a6c41e4153d7e28cb4824d4b',1,'jchess::DrawLocalSettings']]],
  ['selectedhexagon',['selectedHexagon',['../classjchess_1_1game_logic_1_1_chessboard.html#aaf815a1f2c21c6e492d6317a1ebc8dbf',1,'jchess::gameLogic::Chessboard']]],
  ['settings',['settings',['../classjchess_1_1_game_panel.html#a2e2ba1c28f98afd6183b44d3c20c40ed',1,'jchess.GamePanel.settings()'],['../classjchess_1_1_settings.html#ab4dd315a21dbb01cc25cae7bca4bf05c',1,'jchess.Settings.settings()'],['../classjchess_1_1test_1_1_test_settings.html#a2295d4423be56a50ef154726ab671bd3',1,'jchess.test.TestSettings.settings()']]],
  ['sizeofchessboard',['sizeOfChessboard',['../classjchess_1_1_game_panel.html#acff1add8d8b8514b27b445b1f0371391',1,'jchess::GamePanel']]],
  ['status',['status',['../classjchess_1_1_player.html#ae38c91a7953977d648ac3f464d693d12',1,'jchess::Player']]]
];
