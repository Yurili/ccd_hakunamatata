var searchData=
[
  ['id',['id',['../classjchess_1_1_player.html#a50b161fd16a925d03408a248c1bd913d',1,'jchess::Player']]],
  ['idle',['idle',['../enumjchess_1_1_player_1_1_s_t_a_t_u_s.html#a5dbbf08a3426595d51f5668d52feffc1',1,'jchess::Player::STATUS']]],
  ['inkreisradius',['inKreisRadius',['../classjchess_1_1_game_panel.html#a5623bb717f1cd99c14ad3c47e95c5249',1,'jchess::GamePanel']]],
  ['instance',['instance',['../classjchess_1_1game_logic_1_1_geometric_operations.html#a9b3eab8d35254834ff63e1836d8458fb',1,'jchess::gameLogic::GeometricOperations']]],
  ['iterative',['iterative',['../classjchess_1_1game_logic_1_1_movement_pattern.html#affd651c47c3295bb0ce8c075ddad8886',1,'jchess::gameLogic::MovementPattern']]]
];
