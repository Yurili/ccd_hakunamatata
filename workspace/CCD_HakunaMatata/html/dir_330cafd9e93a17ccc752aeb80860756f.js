var dir_330cafd9e93a17ccc752aeb80860756f =
[
    [ "gameLogic", "dir_629aa590b5ec0024afe1c6fb312aea27.html", "dir_629aa590b5ec0024afe1c6fb312aea27" ],
    [ "pieces", "dir_94ccd6dfcf601ebc421628afc8b1dd21.html", "dir_94ccd6dfcf601ebc421628afc8b1dd21" ],
    [ "test", "dir_d3595ef4c9cbab935834339b493e27f0.html", "dir_d3595ef4c9cbab935834339b493e27f0" ],
    [ "DrawLocalSettings.java", "_draw_local_settings_8java.html", [
      [ "DrawLocalSettings", "classjchess_1_1_draw_local_settings.html", "classjchess_1_1_draw_local_settings" ]
    ] ],
    [ "GamePanel.java", "_game_panel_8java.html", [
      [ "GamePanel", "classjchess_1_1_game_panel.html", "classjchess_1_1_game_panel" ]
    ] ],
    [ "ImageLoader.java", "_image_loader_8java.html", [
      [ "ImageLoader", "classjchess_1_1_image_loader.html", "classjchess_1_1_image_loader" ]
    ] ],
    [ "JChessAboutBox.java", "_j_chess_about_box_8java.html", [
      [ "JChessAboutBox", "classjchess_1_1_j_chess_about_box.html", "classjchess_1_1_j_chess_about_box" ]
    ] ],
    [ "JChessApp.java", "_j_chess_app_8java.html", [
      [ "JChessApp", "classjchess_1_1_j_chess_app.html", "classjchess_1_1_j_chess_app" ]
    ] ],
    [ "JChessView.java", "_j_chess_view_8java.html", [
      [ "JChessView", "classjchess_1_1_j_chess_view.html", "classjchess_1_1_j_chess_view" ]
    ] ],
    [ "NewGameWindow.java", "_new_game_window_8java.html", [
      [ "NewGameWindow", "classjchess_1_1_new_game_window.html", "classjchess_1_1_new_game_window" ]
    ] ],
    [ "Player.java", "_player_8java.html", [
      [ "Player", "classjchess_1_1_player.html", "classjchess_1_1_player" ],
      [ "STATUS", "enumjchess_1_1_player_1_1_s_t_a_t_u_s.html", "enumjchess_1_1_player_1_1_s_t_a_t_u_s" ]
    ] ],
    [ "Settings.java", "_settings_8java.html", [
      [ "Settings", "classjchess_1_1_settings.html", "classjchess_1_1_settings" ]
    ] ]
];