var classjchess_1_1_j_chess_view =
[
    [ "JChessView", "classjchess_1_1_j_chess_view.html#ab9f754128f22df57240c6e65edaf05d2", null ],
    [ "actionPerformed", "classjchess_1_1_j_chess_view.html#a062c6f5138997d9a94fbb29d7a76d7a8", null ],
    [ "getGame", "classjchess_1_1_j_chess_view.html#a323088a308406db5b87a6d51e29e12f2", null ],
    [ "getNewGameFrame", "classjchess_1_1_j_chess_view.html#affdc71bfb858130e03ea79115913b0ae", null ],
    [ "initComponents", "classjchess_1_1_j_chess_view.html#a1750bf267505ec9cacc170f7f85aa849", null ],
    [ "setNewGameFrame", "classjchess_1_1_j_chess_view.html#a3caa8b2d388a995be6e390a3b9f15838", null ],
    [ "showAboutBox", "classjchess_1_1_j_chess_view.html#a6a185175d51f2f6076d8ea1662341a8e", null ],
    [ "aboutBox", "classjchess_1_1_j_chess_view.html#ad26830ed64de75e2f554869824e11b10", null ],
    [ "fixDimension", "classjchess_1_1_j_chess_view.html#a261338b941a0d1aa5b90fbe8cde34dbd", null ],
    [ "menuBar", "classjchess_1_1_j_chess_view.html#a5b0d147399972ac42e28a62ef1477da9", null ],
    [ "newGame", "classjchess_1_1_j_chess_view.html#ab915a14d8c38ea10112ac0946f1fff24", null ],
    [ "newGameFrame", "classjchess_1_1_j_chess_view.html#a50e81977796444f79f4aae6ca7e95218", null ],
    [ "newGameItem", "classjchess_1_1_j_chess_view.html#a6544f1fa8caaa62ed3a4d12b857283d4", null ]
];