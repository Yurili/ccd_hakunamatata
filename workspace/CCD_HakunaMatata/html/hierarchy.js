var hierarchy =
[
    [ "jchess.gameLogic.Chessboard", "classjchess_1_1game_logic_1_1_chessboard.html", null ],
    [ "jchess.gameLogic.ChessVector", "classjchess_1_1game_logic_1_1_chess_vector.html", null ],
    [ "jchess.gameLogic.GeometricOperations", "classjchess_1_1game_logic_1_1_geometric_operations.html", null ],
    [ "jchess.gameLogic.Hexagon", "classjchess_1_1game_logic_1_1_hexagon.html", null ],
    [ "jchess.ImageLoader", "classjchess_1_1_image_loader.html", null ],
    [ "JDialog", null, [
      [ "jchess.JChessAboutBox", "classjchess_1_1_j_chess_about_box.html", null ]
    ] ],
    [ "jchess.gameLogic.Movement", "classjchess_1_1game_logic_1_1_movement.html", null ],
    [ "jchess.gameLogic.MovementPattern", "classjchess_1_1game_logic_1_1_movement_pattern.html", null ],
    [ "jchess.pieces.Piece", "classjchess_1_1pieces_1_1_piece.html", [
      [ "jchess.pieces.Bishop", "classjchess_1_1pieces_1_1_bishop.html", null ],
      [ "jchess.pieces.King", "classjchess_1_1pieces_1_1_king.html", null ],
      [ "jchess.pieces.Knight", "classjchess_1_1pieces_1_1_knight.html", null ],
      [ "jchess.pieces.Pawn", "classjchess_1_1pieces_1_1_pawn.html", null ],
      [ "jchess.pieces.Queen", "classjchess_1_1pieces_1_1_queen.html", null ],
      [ "jchess.pieces.Rook", "classjchess_1_1pieces_1_1_rook.html", null ]
    ] ],
    [ "jchess.Player.STATUS", "enumjchess_1_1_player_1_1_s_t_a_t_u_s.html", null ],
    [ "jchess.test.TestChessVector", "classjchess_1_1test_1_1_test_chess_vector.html", null ],
    [ "jchess.test.TestGamePanel", "classjchess_1_1test_1_1_test_game_panel.html", null ],
    [ "jchess.test.TestGeometricOperations", "classjchess_1_1test_1_1_test_geometric_operations.html", null ],
    [ "jchess.test.TestMovement", "classjchess_1_1test_1_1_test_movement.html", null ],
    [ "jchess.test.TestSettings", "classjchess_1_1test_1_1_test_settings.html", null ],
    [ "ActionListener", null, [
      [ "jchess.DrawLocalSettings", "classjchess_1_1_draw_local_settings.html", null ],
      [ "jchess.JChessView", "classjchess_1_1_j_chess_view.html", null ]
    ] ],
    [ "FrameView", null, [
      [ "jchess.JChessView", "classjchess_1_1_j_chess_view.html", null ]
    ] ],
    [ "JDialog", null, [
      [ "jchess.NewGameWindow", "classjchess_1_1_new_game_window.html", null ]
    ] ],
    [ "JPanel", null, [
      [ "jchess.DrawLocalSettings", "classjchess_1_1_draw_local_settings.html", null ],
      [ "jchess.GamePanel", "classjchess_1_1_game_panel.html", null ]
    ] ],
    [ "MouseListener", null, [
      [ "jchess.GamePanel", "classjchess_1_1_game_panel.html", null ]
    ] ],
    [ "Serializable", null, [
      [ "jchess.Player", "classjchess_1_1_player.html", null ],
      [ "jchess.Settings", "classjchess_1_1_settings.html", null ]
    ] ],
    [ "SingleFrameApplication", null, [
      [ "jchess.JChessApp", "classjchess_1_1_j_chess_app.html", null ]
    ] ],
    [ "TextListener", null, [
      [ "jchess.DrawLocalSettings", "classjchess_1_1_draw_local_settings.html", null ]
    ] ]
];