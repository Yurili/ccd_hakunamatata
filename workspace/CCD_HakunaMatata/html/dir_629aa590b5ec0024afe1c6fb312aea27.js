var dir_629aa590b5ec0024afe1c6fb312aea27 =
[
    [ "Chessboard.java", "_chessboard_8java.html", [
      [ "Chessboard", "classjchess_1_1game_logic_1_1_chessboard.html", "classjchess_1_1game_logic_1_1_chessboard" ]
    ] ],
    [ "ChessVector.java", "_chess_vector_8java.html", [
      [ "ChessVector", "classjchess_1_1game_logic_1_1_chess_vector.html", "classjchess_1_1game_logic_1_1_chess_vector" ]
    ] ],
    [ "GeometricOperations.java", "_geometric_operations_8java.html", [
      [ "GeometricOperations", "classjchess_1_1game_logic_1_1_geometric_operations.html", "classjchess_1_1game_logic_1_1_geometric_operations" ]
    ] ],
    [ "Hexagon.java", "_hexagon_8java.html", [
      [ "Hexagon", "classjchess_1_1game_logic_1_1_hexagon.html", "classjchess_1_1game_logic_1_1_hexagon" ]
    ] ],
    [ "Movement.java", "_movement_8java.html", [
      [ "Movement", "classjchess_1_1game_logic_1_1_movement.html", "classjchess_1_1game_logic_1_1_movement" ]
    ] ],
    [ "MovementPattern.java", "_movement_pattern_8java.html", [
      [ "MovementPattern", "classjchess_1_1game_logic_1_1_movement_pattern.html", "classjchess_1_1game_logic_1_1_movement_pattern" ]
    ] ]
];