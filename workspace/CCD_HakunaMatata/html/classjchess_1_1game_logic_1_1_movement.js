var classjchess_1_1game_logic_1_1_movement =
[
    [ "Movement", "classjchess_1_1game_logic_1_1_movement.html#a4690f75565b8a8aaf699d3b4207eacf3", null ],
    [ "checkPlayerStatus", "classjchess_1_1game_logic_1_1_movement.html#af528f729cd2d395e8c68434ce0593da0", null ],
    [ "copyArrayList", "classjchess_1_1game_logic_1_1_movement.html#a63970d5389bb460e2058b193242ffa41", null ],
    [ "getBishopPattern", "classjchess_1_1game_logic_1_1_movement.html#a1106ded1e1314c9f580053dcf80b0af2", null ],
    [ "getKingPattern", "classjchess_1_1game_logic_1_1_movement.html#a2f2e5eb258a555ecaf52540079add9c0", null ],
    [ "getKingPosition", "classjchess_1_1game_logic_1_1_movement.html#aac85c7225984e4a751ca4792c7520e3b", null ],
    [ "getKnightPattern", "classjchess_1_1game_logic_1_1_movement.html#a1a52c7ce415ed03c02a92efb7a7a70df", null ],
    [ "getPawnPattern", "classjchess_1_1game_logic_1_1_movement.html#a227355be6c26b12a08ae244d37d2dfdc", null ],
    [ "getQueenPattern", "classjchess_1_1game_logic_1_1_movement.html#a7e14c1fecad83aa20d6d7e7d68bfd57a", null ],
    [ "getRookPattern", "classjchess_1_1game_logic_1_1_movement.html#a1fce48dbf86aedb254bec074d774398d", null ],
    [ "isFutureStatusIdle", "classjchess_1_1game_logic_1_1_movement.html#acd0709a4c779cb30fa159793612ddbdf", null ],
    [ "updateAllMoves", "classjchess_1_1game_logic_1_1_movement.html#ab5f42b486ac45b797a458dd2839c76f2", null ],
    [ "updateMoves", "classjchess_1_1game_logic_1_1_movement.html#adbc97a61bf74d10b10edfaf2c95b7f53", null ],
    [ "patterns", "classjchess_1_1game_logic_1_1_movement.html#ae2d2a68660fdc0f431421e55a8396b1e", null ]
];