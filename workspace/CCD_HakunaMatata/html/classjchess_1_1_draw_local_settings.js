var classjchess_1_1_draw_local_settings =
[
    [ "actionPerformed", "classjchess_1_1_draw_local_settings.html#a05f6cb4f7f2cafdee1e6dda0db957170", null ],
    [ "textValueChanged", "classjchess_1_1_draw_local_settings.html#a554de3a7f2d0b398355a86fadaa0361f", null ],
    [ "trimString", "classjchess_1_1_draw_local_settings.html#a9ecaa08422b31e8f5573462936ecb226", null ],
    [ "firstName", "classjchess_1_1_draw_local_settings.html#af8a1bcf78e427560bcd4168e89cac47c", null ],
    [ "firstNameColor", "classjchess_1_1_draw_local_settings.html#abf4c15edc05e8ab708fad321be65d3eb", null ],
    [ "firstNameLab", "classjchess_1_1_draw_local_settings.html#a1749669e711dd1f466fdbeba3478474e", null ],
    [ "gbc", "classjchess_1_1_draw_local_settings.html#a2114727f09ece8e962760813853820fb", null ],
    [ "gbl", "classjchess_1_1_draw_local_settings.html#ac56ea4f289a2aff18d9b4382621fd381", null ],
    [ "okButton", "classjchess_1_1_draw_local_settings.html#a26a7a670250296e723c3e1d881a0a7f8", null ],
    [ "parent", "classjchess_1_1_draw_local_settings.html#aef7b47db871335e9e18ccd55f226f4de", null ],
    [ "secondName", "classjchess_1_1_draw_local_settings.html#a30d7c896a73fbbdba9d64797e66c5ede", null ],
    [ "secondNameColor", "classjchess_1_1_draw_local_settings.html#aa0face1e448115f394e6389383b5cc58", null ],
    [ "secondNameLab", "classjchess_1_1_draw_local_settings.html#a9d19d398a6c41e4153d7e28cb4824d4b", null ],
    [ "thirdName", "classjchess_1_1_draw_local_settings.html#ac735ea7d67dc7107314f4f490afa3cc9", null ],
    [ "thirdNameColor", "classjchess_1_1_draw_local_settings.html#a8d60a8105f67812ec453f2e1a750a8b1", null ],
    [ "thirdNameLab", "classjchess_1_1_draw_local_settings.html#aa75a75bb870c719808d60fb817529cec", null ]
];