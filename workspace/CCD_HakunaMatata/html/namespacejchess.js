var namespacejchess =
[
    [ "gameLogic", "namespacejchess_1_1game_logic.html", "namespacejchess_1_1game_logic" ],
    [ "pieces", "namespacejchess_1_1pieces.html", "namespacejchess_1_1pieces" ],
    [ "test", "namespacejchess_1_1test.html", "namespacejchess_1_1test" ],
    [ "DrawLocalSettings", "classjchess_1_1_draw_local_settings.html", "classjchess_1_1_draw_local_settings" ],
    [ "GamePanel", "classjchess_1_1_game_panel.html", "classjchess_1_1_game_panel" ],
    [ "ImageLoader", "classjchess_1_1_image_loader.html", "classjchess_1_1_image_loader" ],
    [ "JChessAboutBox", "classjchess_1_1_j_chess_about_box.html", "classjchess_1_1_j_chess_about_box" ],
    [ "JChessApp", "classjchess_1_1_j_chess_app.html", "classjchess_1_1_j_chess_app" ],
    [ "JChessView", "classjchess_1_1_j_chess_view.html", "classjchess_1_1_j_chess_view" ],
    [ "NewGameWindow", "classjchess_1_1_new_game_window.html", "classjchess_1_1_new_game_window" ],
    [ "Player", "classjchess_1_1_player.html", "classjchess_1_1_player" ],
    [ "Settings", "classjchess_1_1_settings.html", "classjchess_1_1_settings" ]
];