/*
# Three Person Chess
# ------------------
#
# developed by: HakunaMatata
# workers: Anne Reich, Eric Schubert, Chris Taggeselle
*/

package jchess.gameLogic;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import jchess.ImageLoader;
import jchess.Player;
import jchess.pieces.Bishop;
import jchess.pieces.King;
import jchess.pieces.Knight;
import jchess.pieces.Pawn;
import jchess.pieces.Queen;
import jchess.pieces.Rook;

/** Class to represent chessboard. Chessboard is made from squares.
 * It is setting the squers of chessboard and sets the pieces(pawns)
 * witch the owner is current player on it.
 * 
 * @author HakunaMatata
 */
public class Chessboard  {
	
    public static final int TOP = 0;
    public static final int BOTTOM = 14;
    
    
    private ArrayList<Hexagon> listHexagons = new ArrayList<Hexagon>();
    private Hexagon selectedHexagon;
    private Set<ChessVector> targetHexagons;

    public static final int BOARD_EDGE = 8;
    public static final int BOARD_VEC_MIN = -7;
    public static final int BOARD_VEC_MAX = 7;
    
    public ArrayList<Hexagon> getListHexagons() {
		return listHexagons;
	}

	/** 
	 * Chessboard class constructor.
	 * Creates list of hexagons on the right position.
     */
    public Chessboard() {
        this.selectedHexagon = null;
        this.targetHexagons = new HashSet<ChessVector>();

        for (int b = 0; b < BOARD_EDGE; b++) {
    		for (int a = BOARD_VEC_MIN; (a + b <= BOARD_VEC_MAX); a++) {    			
    			listHexagons.add(new Hexagon(a, b, null));

    			if (b != 0) {
	        		if (a + (-b) >= (BOARD_VEC_MIN)) {
	        			listHexagons.add(new Hexagon(a, (-b), null));
	        		} 
	        		else {
	        			listHexagons.add(new Hexagon((-a), (-b), null));
	        		}
    			}
        	}
    	}
    }

    /** 
     * Method setPieces on begin of new game.
     * @param plOne reference to white player
     * @param plTwo reference to green player
     * @param plThree reference to black player
     */
    public void setPieces(Player plOne, Player plTwo, Player plThree) {

		for (int i = 0; i <= 8; i++) {
			// black Pawns
	    	getHexagon(-6, i - 1).setPiece(new Pawn(ImageLoader.loadImage("Pawn-B.png"), plThree));
	    	// white Pawns
	    	getHexagon(i - 1, -6).setPiece(new Pawn(ImageLoader.loadImage("Pawn-W.png"), plOne));
	    	// green Pawns
	    	getHexagon(7 - i, i - 1).setPiece(new Pawn(ImageLoader.loadImage("Pawn-G.png"), plTwo));
	    }
    	
		this.getHexagon(-7, 0).setPiece(new Rook(ImageLoader.loadImage("Rook-B.png"), plThree));
		this.getHexagon(-7, 7).setPiece(new Rook(ImageLoader.loadImage("Rook-B.png"), plThree));
		this.getHexagon(-7, 1).setPiece(new Knight(ImageLoader.loadImage("Knight-B.png"), plThree));
		this.getHexagon(-7, 6).setPiece(new Knight(ImageLoader.loadImage("Knight-B.png"), plThree));
		this.getHexagon(-7, 2).setPiece(new Bishop(ImageLoader.loadImage("Bishop-B.png"), plThree));
		this.getHexagon(-7, 5).setPiece(new Bishop(ImageLoader.loadImage("Bishop-B.png"), plThree));
		this.getHexagon(-7, 4).setPiece(new Queen(ImageLoader.loadImage("Queen-B.png"), plThree));
		this.getHexagon(-7, 3).setPiece(new King(ImageLoader.loadImage("King-B.png"), plThree));
		
		this.getHexagon(0, -7).setPiece(new Rook(ImageLoader.loadImage("Rook-W.png"), plOne));
		this.getHexagon(7, -7).setPiece(new Rook(ImageLoader.loadImage("Rook-W.png"), plOne));
		this.getHexagon(1, -7).setPiece(new Knight(ImageLoader.loadImage("Knight-W.png"), plOne));
		this.getHexagon(6, -7).setPiece(new Knight(ImageLoader.loadImage("Knight-W.png"), plOne));
		this.getHexagon(2, -7).setPiece(new Bishop(ImageLoader.loadImage("Bishop-W.png"), plOne));
		this.getHexagon(5, -7).setPiece(new Bishop(ImageLoader.loadImage("Bishop-W.png"), plOne));
		this.getHexagon(4, -7).setPiece(new Queen(ImageLoader.loadImage("Queen-W.png"), plOne));
		this.getHexagon(3, -7).setPiece(new King(ImageLoader.loadImage("King-W.png"), plOne));

		this.getHexagon(0, 7).setPiece(new Rook(ImageLoader.loadImage("Rook-G.png"), plTwo));
		this.getHexagon(7, 0).setPiece(new Rook(ImageLoader.loadImage("Rook-G.png"), plTwo));
		this.getHexagon(1, 6).setPiece(new Knight(ImageLoader.loadImage("Knight-G.png"), plTwo));
		this.getHexagon(6, 1).setPiece(new Knight(ImageLoader.loadImage("Knight-G.png"), plTwo));
		this.getHexagon(2, 5).setPiece(new Bishop(ImageLoader.loadImage("Bishop-G.png"), plTwo));
		this.getHexagon(5, 2).setPiece(new Bishop(ImageLoader.loadImage("Bishop-G.png"), plTwo));
		this.getHexagon(3, 4).setPiece(new Queen(ImageLoader.loadImage("Queen-G.png"), plTwo));
		this.getHexagon(4, 3).setPiece(new King(ImageLoader.loadImage("King-G.png"), plTwo));
    }
    
    /**
     * Click on hexagon in JPanel returns the hexagon in the list.
     * @param x pixel of mouse click 
     * @param y pixel of mouse click 
     * @return the clicked hexagon
     */    
    public Hexagon getHexagonByPixel(int x, int y) {
        for (Hexagon elem  : this.listHexagons) {
        	if (elem.getGeometricHexagon().contains(x, y)) {
        		return elem;
		 	}
        }
        return null;
    }
        
    /** 
     * method to get hexagon by indizes.
     * @param a translation in A direction 
     * @param b translation in B direction
     * @return target hexagon
     */
    public Hexagon getHexagon(int a, int b) {
    	for (Hexagon hex : this.listHexagons) {
    		if (hex.getPozX() == a && hex.getPozY() == b) {
    			return hex;
    		}
    	}
    	return null;
    }

  
	public Hexagon getSelectedHexagon() {
		return selectedHexagon;
	}

	public void setSelectedHexagon(Hexagon selectedHexagon) {
		this.selectedHexagon = selectedHexagon;
	}
	
	/**
	 * remove the clicked hexagon on chessboard.
	 */
	public void deleteSelectedHexagon() {
		this.selectedHexagon = null;
	}

	public Set<ChessVector> getTargetHexagons() {
		return targetHexagons;
	}

	public void setTargetHexagons(Set<ChessVector> targetHexagons) {
		this.targetHexagons = targetHexagons;
	}
	
	/**
	 * remove the highlighted targets of the chessboard.
	 */
	public void deleteTargetHexagons() {
		this.targetHexagons = new HashSet<ChessVector>();
	}
    
}
