/*
# Three Person Chess
# ------------------
#
# developed by: HakunaMatata
# workers: Anne Reich, Eric Schubert, Chris Taggeselle
*/

package jchess.gameLogic;

/** Vector datatype for describing movement and positions on the hexagonal chessboard.
 * 
 * @author Eric Schubert
 *
 */
public class ChessVector {

	private int x;
	private int y;

	/** creates a vector of (x,y).
	 * 
	 * @param x x-value
	 * @param y y-value
	 */
	public ChessVector(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	/** empty constructor creates a vector of (0,0).
	 * 
	 */
	public ChessVector() {
		this.x = 0;
		this.y = 0;
	}
	
	/** adds a second vector onto this.
	 * 
	 * @param vec second vector to add on this
	 */
	public void add(ChessVector vec) {
		this.x += vec.getX();
		this.y += vec.getY();
	}
	
	/** calculates scalar multiplication.
	 * 
	 * @param scalar value for scalar multiplication
	 * @return result of scalar multiplication
	 */
	public ChessVector times(int scalar) {
		final ChessVector result = new ChessVector(this.x * scalar, this.y * scalar);
		
		return result;
	}
	
	/** calculates vector sum.
	 * 
	 * @param a first input Vector
	 * @param b second input Vector
	 * @return sum of both input vectors
	 */
	public static ChessVector sumOfVectors(ChessVector a, ChessVector b) {
		
		final ChessVector result = new ChessVector(a.getX() + b.getX(), a.getY() + b.getY());
		
		return result;
	}
	
	/** checker method, if a ChessVector is an diagonal vector. On a hexagon
	 * this means another hexagon reached along edges.
	 * 
	 * @return true, if it's an diagonal hexagon
	 */
	public boolean isDiagonal() {
		
		if (this.x == this.y) {
			return true;
		} 
		else if (this.x * 2 == -this.y || -this.x == this.y * 2) {
			return true;
		} 
		else {
			return false;
		}
	}
	
	@Override
	public String toString() {		
		return "(" + this.x + ", " + this.y + ")";		
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if (obj == null) {
			return false;
		}
		else if (obj.getClass() != this.getClass()) {
			return false;
		}
		else {			
			final ChessVector vector = (ChessVector) obj;
			
			return this.x == vector.getX() && this.y == vector.getY();
		}
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}
	
	public int getX() {
		return x;
	}
	
	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}
	
	public void setY(int y) {
		this.y = y;
	}	
}
