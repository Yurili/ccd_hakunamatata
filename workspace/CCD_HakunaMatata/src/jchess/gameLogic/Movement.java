/*
# Three Person Chess
# ------------------
#
# developed by: HakunaMatata
# workers: Anne Reich, Eric Schubert, Chris Taggeselle
*/

package jchess.gameLogic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import jchess.Player;
import jchess.Settings;
import jchess.pieces.Bishop;
import jchess.pieces.King;
import jchess.pieces.Knight;
import jchess.pieces.Pawn;
import jchess.pieces.Piece;
import jchess.pieces.Rook;

/**This class calculates all possible Movements of the figures
 * of the 3-Person-Chess.
 * 
 * White base: top
 * Black base: bottom left
 * Green base: bottom right
 * 
 *      /\
 *     /  \
 *    /    \
 *    |    |  --> positive a direction
 *    |    |
 *    \    /
 *     \  / -> positive b direction
 *      \/
 * 
 * @author Eric Schubert
 *
 */
public class Movement {

	private HashMap<String, MovementPattern> patterns = null;
	
	/** 
	 * constructor for movement object.
	 */
	public Movement() {
		
		this.patterns = new HashMap<String, MovementPattern>();
		
		// Create all Movement patterns
		String piece;
		MovementPattern pattern;
		
		//Pawns
		piece = "pl1Pawn";
		pattern = getPawnPattern(1);
		this.patterns.put(piece, pattern);
		
		piece = "pl2Pawn";
		pattern = getPawnPattern(2);
		this.patterns.put(piece, pattern);
		
		piece = "pl3Pawn";
		pattern = getPawnPattern(3);
		this.patterns.put(piece, pattern);		
		
		//King
		piece = "King";
		pattern = getKingPattern();
		this.patterns.put(piece, pattern);
		
		//Knight
		piece = "Knight";
		pattern = getKnightPattern();
		this.patterns.put(piece, pattern);
		
		//Rook
		piece = "Rook";
		pattern = getRookPattern();
		this.patterns.put(piece, pattern);
		
		//Bishop		
		piece = "Bishop";
		pattern = getBishopPattern();
		this.patterns.put(piece, pattern);
		
		//Queen
		piece = "Queen";
		pattern = getQueenPattern();
		this.patterns.put(piece, pattern);
	}
	
	/** 
	 * Update all moves of all pieces on the board.
	 * 
	 * @param chess The linked chessboard
	 */
	public void updateAllMoves(Chessboard chess) {
		
		boolean plOneMovesLeft = false;
		boolean plTwoMovesLeft = false;
		boolean plThreeMovesLeft = false;
		
		for (Hexagon hex : chess.getListHexagons()) {
			
			if (!hex.isEmpty()) {
				hex.getPiece().setAllMoves(updateMoves(hex, chess));
				
				// Check, if anyone has no moves at all --> checkmate
				if (!plOneMovesLeft) {
					if (hex.getPiece().getPlayer().getId() == 1 
							  && !hex.getPiece().getAllMoves().isEmpty()) 
					{
						plOneMovesLeft = true;
					}					
				} 
				else if (!plTwoMovesLeft) {
					if (hex.getPiece().getPlayer().getId() == 2 
							  && !hex.getPiece().getAllMoves().isEmpty()) 
					{
						plTwoMovesLeft = true;
					}
				}
				else if (!plThreeMovesLeft) {
					if (hex.getPiece().getPlayer().getId() == 3 
							  && !hex.getPiece().getAllMoves().isEmpty()) 
					{
						plThreeMovesLeft = true;
					}
				}
			}			
		}
		
		Settings.getPlayerByID(1).setStatus(checkPlayerStatus(chess.getListHexagons(), 1));
		Settings.getPlayerByID(2).setStatus(checkPlayerStatus(chess.getListHexagons(), 2));
		Settings.getPlayerByID(3).setStatus(checkPlayerStatus(chess.getListHexagons(), 3));
		
		if (!plOneMovesLeft) {
			Settings.getPlayerByID(1).setStatus(Player.STATUS.checkmated);
		}
		if (!plTwoMovesLeft) { 
			Settings.getPlayerByID(2).setStatus(Player.STATUS.checkmated);
		}
		if (!plThreeMovesLeft) {
			Settings.getPlayerByID(3).setStatus(Player.STATUS.checkmated);
		}
	}
	
	/**
	 * Update possible moves for one special hexagon.
	 * 
	 * @param hex	the special hexagon
	 * @param chess	the linked chessboard
	 * @return a set of vectors of all possible movement options
	 */
	private Set<ChessVector> updateMoves(Hexagon hex, Chessboard chess) {		
		
		final Set<ChessVector> allNewMoves = new HashSet<ChessVector>();
		ChessVector result = new ChessVector();
		final ChessVector position = new ChessVector(hex.getPozX(), hex.getPozY());		
		final Piece piece = hex.getPiece();
		final Player activePlayer = piece.getPlayer();
		
		MovementPattern pattern;
		
		// Gets correct MovementPattern
		if (piece instanceof Pawn) {
			if (piece.getPlayer().getId() == 2) {
				pattern = this.patterns.get("pl2Pawn");
			} 
			else if (piece.getPlayer().getId() == 3) {
				pattern = this.patterns.get("pl3Pawn");
			} 
			else {
				pattern = this.patterns.get("pl1Pawn");
			}				
		} 
		else if (piece instanceof Rook) {
			pattern = this.patterns.get("Rook");
		} 
		else if (piece instanceof King) {
			pattern = this.patterns.get("King");
		}
		else if (piece instanceof Knight) {
			pattern = this.patterns.get("Knight");
		} 
		else if (piece instanceof Bishop) {
			pattern = this.patterns.get("Bishop");
		} 
		else {
			pattern = this.patterns.get("Queen");
		}
		
		// Check for reachable fields
		if (piece instanceof Pawn) {
			// Only Pawns have different Striking and Movement rules
			for (ChessVector v: pattern.getMovingOptions()) {			
				
				result = ChessVector.sumOfVectors(v, position);
				
				final Hexagon target = chess.getHexagon(result.getX(), result.getY());
								
				if (target != null && target.isEmpty()) {
					
					// Test ahead, if after possible move the player remains unchecked					
					if (isFutureStatusIdle(chess.getListHexagons(), activePlayer.getId(), target, hex, piece)) {
						allNewMoves.add(result);
					}
				}		
			}
			
			for (ChessVector v: pattern.getStrikingOptions()) {
				
				result = ChessVector.sumOfVectors(v, position);
				
				final Hexagon target = chess.getHexagon(result.getX(), result.getY());
				
				if (target != null && !target.isEmpty() && target.getPiece().getPlayer() != piece.getPlayer()) {
					
					// Test ahead, if after possible move the player remains unchecked
					if (isFutureStatusIdle(chess.getListHexagons(), activePlayer.getId(), target, hex, piece)) {
						allNewMoves.add(result);
					}
				}
			}
		}
		
		// All Pieces except Pawns move and strike similar
		else {
			
			// Queen, Rook and Bishop move iterative
			if (pattern.isIterative()) {
				
				for (ChessVector v: pattern.getMovingOptions()) {
					
					int count = 1;
					
					while (true) {
						final ChessVector vector = v.times(count);
						
						// For non-diagonal vectors simple iteration is possible
						if (!v.isDiagonal()) {
							
							result = ChessVector.sumOfVectors(vector, position);							
							final Hexagon target = chess.getHexagon(result.getX(), result.getY());
							
							if (target != null) {
								if (target.isEmpty()) {
									
									// Test ahead, if after possible move the player remains unchecked
									if (isFutureStatusIdle(chess.getListHexagons(), activePlayer.getId(), target, hex, piece)) {
										allNewMoves.add(result);
									}
								} 
								else {
									if (target.getPiece().getPlayer() != piece.getPlayer()) {
										
										// Test ahead, if after possible move the player remains unchecked
										if (isFutureStatusIdle(chess.getListHexagons(), activePlayer.getId(), target, hex, piece)) {
											allNewMoves.add(result);
										}							
									}
									break;
								}
							} 
							else {
								break;
							}
						}
						
						// for diagonal vectors a check for "walls" is needed
						else {
							
							final ChessVector towall1 = new ChessVector();
							final ChessVector towall2 = new ChessVector();
							
							if (v.getX() == v.getY()) {								
								towall1.setX(-v.getX());
								towall1.setY(0);
								towall2.setX(0);
								towall2.setY(-v.getX());
							} 
							else if (v.getX() * 2 == -v.getY()) {								
								towall1.setX(0);
								towall1.setY(v.getX());
								towall2.setX(-v.getX());
								towall2.setY(v.getX());
							}
							else {								
								towall1.setX(v.getY());
								towall1.setY(0);
								towall2.setX(v.getY());
								towall2.setY(-v.getY());
							}
							
							result = ChessVector.sumOfVectors(vector, position);
							final ChessVector wall1 = ChessVector.sumOfVectors(result, towall1);
							final ChessVector wall2 = ChessVector.sumOfVectors(result, towall2);
							
							final Hexagon target = chess.getHexagon(result.getX(), result.getY());
							final Hexagon wallHex1 = chess.getHexagon(wall1.getX(), wall1.getY());
							final Hexagon wallHex2 = chess.getHexagon(wall2.getX(), wall2.getY());
							
							if (target != null) {
								if (wallHex1.isEmpty() || wallHex2.isEmpty()) {
									if (target.isEmpty()) {
										
										// Test ahead, if after possible move the player remains unchecked
										if (isFutureStatusIdle(chess.getListHexagons(), activePlayer.getId(), target, hex, piece)) {
											allNewMoves.add(result);
										}
									} 
									else {
										if (target.getPiece().getPlayer() != piece.getPlayer()) {
											
											// Test ahead, if after possible move the player remains unchecked
											if (isFutureStatusIdle(chess.getListHexagons(), activePlayer.getId(), target, hex, piece)) {
												allNewMoves.add(result);
											}							
										}
										break;
									}
								} 
								else {
									break;
								}
							}
							else {
								break;
							}
						}
						
						count++;
					}
				}			
				
			}
			
			// Knights and Kings move directly
			else {
				for (ChessVector v: pattern.getMovingOptions()) {			
					
					result = ChessVector.sumOfVectors(v, position);
					
					final Hexagon target = chess.getHexagon(result.getX(), result.getY());
					
					if (target != null) {
						if (target.isEmpty()) {
							
							// Test ahead, if after possible move the player remains unchecked
							if (isFutureStatusIdle(chess.getListHexagons(), activePlayer.getId(), target, hex, piece)) {
								allNewMoves.add(result);
							}				
						} 
						else {
							if (target.getPiece().getPlayer() != piece.getPlayer()) {
								
								// Test ahead, if after possible move the player remains unchecked
								if (isFutureStatusIdle(chess.getListHexagons(), activePlayer.getId(), target, hex, piece)) {
									allNewMoves.add(result);
								}
							}
						}
					}			
				}
			}			
		}		
		
		return allNewMoves;
	}
	
	/**
	 * returns the kings position of a specific player.
	 * @param playerid playerid the id of the player
	 * @param fields The Hexagons of the chessboard
	 * @return the ChessVector for the Kings position
	 */
	private ChessVector getKingPosition(int playerid, ArrayList<Hexagon> fields) {
		
		ChessVector result = new ChessVector();
		
		for (Hexagon hex : fields) {
			if (!hex.isEmpty() && hex.getPiece() instanceof King 
					  && hex.getPiece().getPlayer().getId() == playerid) 
			{
				result = hex.getPositionVector();
			}
		}
		
		return result;
	}
	
	/**
	 * Resolves the status of specific player.
	 * @param fields The Hexagons of the chessboard
	 * @param playerid playerid the id of the player
	 * @return status from player (check or idle)
	 */
	private Player.STATUS checkPlayerStatus(ArrayList<Hexagon> fields, int playerid) {
		
		final ChessVector kingPosition = getKingPosition(playerid, fields);
		boolean checked = false;
		
		for (Hexagon hex : fields) {
			
			if (!hex.isEmpty() && hex.getPiece().getPlayer().getId() != playerid) {
				for (ChessVector vec : hex.getPiece().getAllMoves()) {
					if (vec.equals(kingPosition)) {
						checked = true;
					}
				}
			}			
		}
		
		if (checked) {
			return Player.STATUS.checked;
		} 
		else {
			return Player.STATUS.idle;
		}
	}
	
	/** delivers the 3 different Pawn Patterns.
	 * 
	 * @param playerid The id of the player the pawns belong to
	 * @return the specific pawn pattern
	 */
	private MovementPattern getPawnPattern(int playerid) {
		
		final MovementPattern pattern = new MovementPattern(false);		
		
		if (playerid == 1) {
			pattern.addMoveOption(-1, 1);
			pattern.addMoveOption(0, 1);
			pattern.addStrikeOption(1, 1);
			pattern.addStrikeOption(-2, 1);
			pattern.addStrikeOption(-1, 2);		
		} 
		else if (playerid == 2) {
			pattern.addMoveOption(0, -1);
			pattern.addMoveOption(-1, 0);
			pattern.addStrikeOption(-2, 1);
			pattern.addStrikeOption(-1, -1);
			pattern.addStrikeOption(1, -2);
		} 
		else if (playerid == 3) {
			pattern.addMoveOption(1, 0);
			pattern.addMoveOption(1, -1);
			pattern.addStrikeOption(2, -1);
			pattern.addStrikeOption(1, -2);
			pattern.addStrikeOption(1, 1);
		} 
		else {
			System.out.println("Wrong color in PawnPattern.");
		}		
		
		return pattern;
	}
	
	/** Delivers king pattern.
	 * 
	 * @return the king pattern
	 */
	private MovementPattern getKingPattern() {
		final MovementPattern pattern = new MovementPattern(false);
		
		pattern.addMoveOption(1, 0);
		pattern.addMoveOption(0, 1);
		pattern.addMoveOption(-1, 0);
		pattern.addMoveOption(0, -1);
		pattern.addMoveOption(1, -1);
		pattern.addMoveOption(-1, 1);
		
		return pattern;
	}
	
	/** Delivers knight pattern.
	 * 
	 * @return the knight pattern
	 */
	private MovementPattern getKnightPattern() {
		final MovementPattern pattern = new MovementPattern(false);
		
		pattern.addMoveOption(2, 1);
		pattern.addMoveOption(1, 2);
		pattern.addMoveOption(-1, 3);
		pattern.addMoveOption(-2, 3);
		pattern.addMoveOption(-3, 1);
		pattern.addMoveOption(-3, 2);
		pattern.addMoveOption(-2, -1);
		pattern.addMoveOption(-1, -2);
		pattern.addMoveOption(1, -3);
		pattern.addMoveOption(2, -3);
		pattern.addMoveOption(3, -1);
		pattern.addMoveOption(3, -2);
		
		return pattern;
	}
	
	/** Delivers rook pattern.
	 * 
	 * @return the rook pattern
	 */
	private MovementPattern getRookPattern() {
		final MovementPattern pattern = new MovementPattern(true);
		
		pattern.addMoveOption(1, 0);
		pattern.addMoveOption(0, 1);
		pattern.addMoveOption(-1, 1);
		pattern.addMoveOption(-1, 0);
		pattern.addMoveOption(0, -1);
		pattern.addMoveOption(1, -1);
		
		return pattern;
	}
	
	/** Delivers bishop pattern.
	 * 
	 * @return the bishop pattern
	 */
	private MovementPattern getBishopPattern() {
		final MovementPattern pattern = new MovementPattern(true);
		
		pattern.addMoveOption(1, 1);
		pattern.addMoveOption(2, -1);
		pattern.addMoveOption(1, -2);
		pattern.addMoveOption(-1, -1);
		pattern.addMoveOption(-2, 1);
		pattern.addMoveOption(-1, 2);
		
		return pattern;
	}
	
	/** Delivers queen pattern.
	 * 
	 * @return the queen pattern
	 */
	private MovementPattern getQueenPattern() {
		final MovementPattern pattern = new MovementPattern(true);
		
		pattern.addMoveOption(1, 0);
		pattern.addMoveOption(0, 1);
		pattern.addMoveOption(-1, 1);
		pattern.addMoveOption(-1, 0);
		pattern.addMoveOption(0, -1);
		pattern.addMoveOption(1, -1);
		pattern.addMoveOption(1, 1);
		pattern.addMoveOption(2, -1);
		pattern.addMoveOption(1, -2);
		pattern.addMoveOption(-1, -1);
		pattern.addMoveOption(-2, 1);
		pattern.addMoveOption(-1, 2);
		
		return pattern;
	}
	
	/** Creates deep clone of an Array List.
	 * 
	 * @param list input list 
	 * @return cloned list
	 */
	private ArrayList<Hexagon> copyArrayList(ArrayList<Hexagon> list) {
		final ArrayList<Hexagon> copy = new ArrayList<Hexagon>();
		
		for (Hexagon hex : list) {
			copy.add((Hexagon) hex.cloneHexagon());
		}
		
		return copy;		
	}
	
	/**
	 * creates a possible future chessboard setup to check the new player status.
	 * @param list original hexagon list from chessboard
	 * @param playerId id from active player
	 * @param target the target hexagon of the future move
	 * @param source the source hexagon of the future move
	 * @param piece the piece to be moved
	 * @return true if future status is idle
	 */
	private boolean isFutureStatusIdle(ArrayList<Hexagon> list, int playerId, Hexagon target, Hexagon source, Piece piece) {
		
		// Test ahead, if after possible move the player remains unchecked
		final ArrayList<Hexagon> supposedSetup = copyArrayList(list);
		for (Hexagon h : supposedSetup) {
			if (target.compareHexagon(h)) {
				h.setPiece(piece);
			}
			if (source.compareHexagon(h)) { 
				h.deletePiece();
			}
		}
		
		return checkPlayerStatus(supposedSetup, playerId) == Player.STATUS.idle;
	}
}
