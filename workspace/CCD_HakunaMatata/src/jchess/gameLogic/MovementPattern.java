/*
# Three Person Chess
# ------------------
#
# developed by: HakunaMatata
# workers: Anne Reich, Eric Schubert, Chris Taggeselle
*/

package jchess.gameLogic;

import java.util.HashSet;
import java.util.Set;

/** 
 * This datatype is used to describe movement options for pieces.
 * 
 * @author Eric Schubert
 *
 */
public class MovementPattern {
	
	private boolean iterative;
	
	private Set<ChessVector> moving = null;
	private Set<ChessVector> onlystriking = null;
	
	/**
	 * 
	 * @param iterative x
	 */
	public MovementPattern(boolean iterative) {
		
		this.iterative = iterative;
		
		this.moving = new HashSet<ChessVector>();
		this.onlystriking = new HashSet<ChessVector>();
	}
	
	/**
	 * 
	 * @return x
	 */
	public boolean isIterative() {
		return this.iterative;
	}
	
	/**
	 * 
	 * @param vec x
	 */
	public void addMoveOption(ChessVector vec) {
		this.moving.add(vec);
	}
	
	/**
	 * 
	 * @param a x
	 * @param b x
	 */
	public void addMoveOption(int a, int b) {
		
		final ChessVector vec = new ChessVector(a, b);
		
		this.moving.add(vec);
	}
	
	/**
	 * 
	 * @param vec x
	 */
	public void addStrikeOption(ChessVector vec) {
		this.onlystriking.add(vec);
	}
	
	/**
	 * 
	 * @param a x
	 * @param b x
	 */
	public void addStrikeOption(int a, int b) {
		
		final ChessVector vec = new ChessVector(a, b);
		
		this.onlystriking.add(vec);
	}
	
	public Set<ChessVector> getMovingOptions() {
		return this.moving;
	}
	
	public Set<ChessVector> getStrikingOptions() {
		return this.onlystriking;
	}
}
