/*
# Three Person Chess
# ------------------
#
# developed by: HakunaMatata
# workers: Anne Reich, Eric Schubert, Chris Taggeselle
*/

package jchess.gameLogic;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;

/** implements necessary scaling and calculation methods.
 * 
 * @author Chris Taggeselle, Eric Schubert, Anne Reich
 *
 */
public final class GeometricOperations {

	private static GeometricOperations instance;

	/**
	 * empty constructor.
	 */
	private GeometricOperations() {

	}

	/**
	 * returns the instance (not null).
	 * @return the required instances 
	 */
	public static GeometricOperations getInstance() {

		if (instance == null) {
			instance = new GeometricOperations();
		}

		return instance;
	}

	/**
	 * calculate the radian value of an angle.
	 * @param degree the degree value of the angle
	 *            
	 * @return the radian value
	 */
	public double getRadiansByAngle(int degree) {
		return degree * (Math.PI / 180);
	}

	/**
	 * returns polygon by given center point, radius and number of verticess.
	 * 
	 * @param x the x value of the center point	             
	 * @param y the y value of the center point
	 * @param outerRadius distance of vertices to center point
	 * @param numberofVertices number of vertices of polygon
	 * @return the created polygon
	 */
	public Polygon getPolygonShapeByPointAndVertices(double x, double y,
			  double outerRadius, int numberofVertices) {
		final Polygon poly = new Polygon();

		for (int i = 0; i < numberofVertices; i++) {
			final int xTemp = (int) (x + outerRadius * Math.cos(i * 2 * Math.PI / numberofVertices + Math.PI
							/ numberofVertices));
			final int yTemp = (int) (y + outerRadius * Math.sin(i * 2 * Math.PI / numberofVertices + Math.PI
							/ numberofVertices));
			poly.addPoint(xTemp, yTemp);
		}
		return poly;
	}

	/**
	 * scales an image to defined resolution.
	 * @param img input image
	 * @param width new image width in pixel
	 * @param height new image height in pixel
	 * @return scaled image
	 */
	public Image scaleImage(Image img, int width, int height) {

		final BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		final Graphics2D graph = (Graphics2D) bi.createGraphics();

		graph.addRenderingHints(new RenderingHints(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY));
		graph.drawImage(img, 0, 0, width, height, null);
		graph.dispose();

		return bi;
	}

}
