/*
# Three Person Chess
# ------------------
#
# developed by: HakunaMatata
# workers: Anne Reich, Eric Schubert, Chris Taggeselle
*/
/*
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Authors:
 * Mateusz Sławomir Lach ( matlak, msl )
 * Damian Marciniak
 */
package jchess.gameLogic;

import java.awt.Polygon;
import java.util.Set;

import jchess.pieces.Piece;

/**
 * Class to represent a chessboard hexagon.
 * @author Team HakunaMatata
 */
public class Hexagon {

    private int pozX; 
    private int pozY; 
    private Piece piece = null; //object Piece on square (and extending Piecie)
    //! each Hexagon has its own geometric parameters for displaying on the JPanel
    private Polygon geometricHexagon; 

    /**
     * Every Hexagon will be created by a given position and a piece on it.
     * @param pozX vector horizontal on chessboard
     * @param pozY vector diagonal on chessboard
     * @param piece piece on the hexagon
     */
    Hexagon(int pozX, int pozY, Piece piece) {
        this.pozX = pozX;
        this.pozY = pozY;
        this.piece = piece;
    } /*--endOf-Square--*/

    /**
     * @param hexagon old hexagon
     */
    Hexagon(Hexagon hexagon) {
        this.pozX = hexagon.getPozX();
        this.pozY = hexagon.getPozY();
        this.piece = hexagon.getPiece();
    }

    /**
     * Copies the current hexagon.
     * @return a copied hexagon with a new pointer.
     */
    public Hexagon cloneHexagon() {
        return new Hexagon(this.pozX, this.pozY, this.piece);
    }
    
    /**
     * Checks whether hexagon contains an piece.
     * @return true is empty
     */
    public boolean isEmpty() {
    	return this.piece == null;
    }   

    /**
     * Compares the hexagons by position horizontal and diagonal.
     * @param hex hexagon which is to be compared
     * @return true if both hexagons are located on the same position.
     */
	public boolean compareHexagon(Hexagon hex) {
    	return (this.pozX == hex.getPozX() && this.pozY == hex.getPozY());
    }
	
	/**
	 * Checks, whether the position of the hexagon is identical with 
	 * an position of the ChessVector.
	 * @param set list of positions on the chessboard
	 * @return true if position is identical
	 */
	public boolean isElementOfVectorSet(Set<ChessVector> set) {
		
		for (ChessVector vec: set) {
			if (this.pozX == vec.getX() && this.pozY == vec.getY()) {
				return true;
			}
		}
		return false;		
	}
    
    public void setGeometricHexagon(Polygon hex) {
    	this.geometricHexagon = hex;
    }
    
    public Polygon getGeometricHexagon() {
    	return this.geometricHexagon;
    }

	public int getPozX() {
		return pozX;
	}

	public int getPozY() {
		return pozY;
	}

	public Piece getPiece() {
		return piece;
	}
	
	public ChessVector getPositionVector() {
		return new ChessVector(pozX, pozY);
	}
	
    public void setPiece(Piece piece) {
        this.piece = piece;
    }
    
    /**
     * remove piece from hexagon.
     */
    public void deletePiece() {
    	this.piece = null;
    }
}
