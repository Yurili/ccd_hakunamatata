/*
# Three Person Chess
# ------------------
#
# developed by: HakunaMatata
# workers: Anne Reich, Eric Schubert, Chris Taggeselle
*/

package jchess;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;

/** Class representings game settings available for the current player.
 * @author HakunaMatata
 */
@SuppressWarnings("serial")
public final class Settings implements Serializable {

    private static ResourceBundle loc = null;

	private static ArrayList<Player> listPlayer = new ArrayList<Player>();
	private static Player activePlayer;
    private static Settings settings;

    /** 
     * Is an Singleton, because the project does not support to create more 
     * than 1 game. 
     * It sets automatically 3 default player.
     * Easy to extend for more players.
     */
    private Settings() {
    	
    	listPlayer.add(new Player("Spieler Weiss"));
    	listPlayer.add(new Player("Spieler Gruen"));
    	listPlayer.add(new Player("Spieler Schwarz"));
    	setActivePlayer(getPlayerByID(1));
    }
    
    /**
     * To get an instance of settings.
     * Apply an instance if no one was created.
     * @return settings
     */
    public static Settings getInstance() {
    	
    	if (settings == null) {
    		settings = new Settings();
    	}
    	
    	return settings;
    }
    
    /**
     * Returns a player by given ID in listPlayer.
     * Example of : TELL DON'T ASK
     * @param id id of the player, which have to be returned
     * @return the last player, of ID is too large
     */
    public static Player getPlayerByID(int id) {
    	
    	if (id > listPlayer.size()) {
    		System.out.println("Player ID " + id + " too large! Return last player in list.");
    		return listPlayer.get(listPlayer.size() - 1);
    	}
    	else {
    		return listPlayer.get(id - 1);
    	}
    		
    }

    /**
	 * 
	 * @param key x
	 * @return x
	 */
	public static String lang(String key) {
        if (Settings.loc == null) {
            Settings.loc = ResourceBundle.getBundle("jchess.resources.i18n.main");
            Locale.setDefault(Locale.ENGLISH);
        }
        String result = "";
        try {
            result = Settings.loc.getString(key);
        }
        catch (java.util.MissingResourceException exc) {
            result = key;
        }
        System.out.println(Settings.loc.getLocale().toString());
        return result;
    }

	public static  Player getActivePlayer() {
		return Settings.activePlayer;
	}

	public static void setActivePlayer(Player activePlayer) {
		Settings.activePlayer = activePlayer;
	}
	
	/** 
	 * Active player finished his round. Switch to the next.
	*/
	public static void switchActivePlayer() {
		
		for (Player player:listPlayer) {
			if (player.getId() == getActivePlayer().getId() && player.getId() < listPlayer.size()) {
				setActivePlayer(listPlayer.get(player.getId()));
				return;
				
			}
			else if (player.getId() == getActivePlayer().getId() && player.getId() == listPlayer.size()) {
				setActivePlayer(getPlayerByID(1));
				return;
				
			}
			
		}		
	}
	
	/*
	 * 
	 * 
	 */
	public static ArrayList<Player> getListPlayer() {
		return listPlayer;
	}
}
