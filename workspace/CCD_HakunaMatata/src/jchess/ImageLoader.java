/*
# Three Person Chess
# ------------------
#
# developed by: HakunaMatata
# workers: Anne Reich, Eric Schubert, Chris Taggeselle
*/
package jchess;

import java.awt.Image;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

/** Class representing the game interface which is seen by a player and
 * where are lockated available for player opptions, current games and where
 * can he start a new game (load it or save it).
 * 
 * @author Anne Reich
 */
public final class ImageLoader {
	
    public static final Properties CONFIGFILE = ImageLoader.getConfigFile();

    /** xxx.
     * 
     */
    private ImageLoader() {
    } /*--endOf-GUI--*/

    /** Method load image by a given name with extension.
     * 
     * @param name string of image to load for ex. "chessboard.jpg"
     * @return image or null if cannot load
     */
    public static Image loadImage(String name) {
        if (CONFIGFILE == null) {
            return null;
        }
        Image img = null;
        URL url = null;
        final Toolkit tk = Toolkit.getDefaultToolkit();
        try {
            final String imageLink = "theme/" + CONFIGFILE.getProperty("THEME", "default") + "/images/" + name;
            url = JChessApp.class.getResource(imageLink);
            img = tk.getImage(url);
        }
        catch (Exception e) {
            System.out.println("some error loading image!");
            e.printStackTrace();
        }
        return img;
    } /*--endOf-loadImage--*/

    /** xxx.
     * 
     * @return ???
     */
    static String getJarPath() {
        String path = ImageLoader.class.getProtectionDomain().getCodeSource().getLocation().getFile();
        path = path.replaceAll("[a-zA-Z0-9%!@#$%^&*\\(\\)\\[\\]\\{\\}\\.\\,\\s]+\\.jar", "");
        final int lastSlash = path.lastIndexOf(File.separator); 
        if (path.length() - 1 == lastSlash) {
            path = path.substring(0, lastSlash);
        }
        path = path.replace("%20", " ");
        return path;
    }

    /** xxx.
     * 
     * @return ???
     */
    static Properties getConfigFile() {
        final Properties defConfFile = new Properties();
        final Properties confFile = new Properties();
        
        InputStream inConfig = null;
        FileOutputStream outFS = null;
        //FileInputStream inFS = null;
        
        final File outFile = new File(ImageLoader.getJarPath() + File.separator + "config.txt");
        try {
        	inConfig = ImageLoader.class.getResourceAsStream("config.txt");
            defConfFile.load(inConfig);
        }
        catch (java.io.IOException exc) {
            System.out.println("some error loading image! what goes: " + exc);
            exc.printStackTrace();
        }
        finally {
        	try {
				inConfig.close();
			} 
        	catch (Exception e) {
        		e.printStackTrace();        		
			}
        }
        if (!outFile.exists()) {
            try {
            	outFS = new FileOutputStream(outFile);
                defConfFile.store(outFS, null);
            }
            catch (Exception exc) {
        		exc.printStackTrace();  
            }
            finally {
            	try {
					outFS.close();
				} 
            	catch (Exception e) {
            		e.printStackTrace();  
				}
            }
        }
        
////        try {   
////        	inFS = new FileInputStream("config.txt");
////            confFile.load(inFS);
////        }
////        catch (Exception exc) {
////    		exc.printStackTrace();  
//        	
//        }
//        finally {
//        	try {
//				inFS.close();
//			} 
//        	catch (Exception e) { 
//        		e.printStackTrace();  
//			}
//        }
//        
        return confFile;
    }

	
}
