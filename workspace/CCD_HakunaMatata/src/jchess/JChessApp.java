/*
# Three Person Chess
# ------------------
#
# developed by: HakunaMatata
# workers: Anne Reich, Eric Schubert, Chris Taggeselle
*/
package jchess;

import org.jdesktop.application.Application;
import org.jdesktop.application.SingleFrameApplication;

/**
 * The main class of the application.
 * @author yurili
 */
public class JChessApp extends SingleFrameApplication {
    private static JChessView jcv;
    /**
     * At startup create and show the main frame of the application.
     */
    @Override protected void startup() {
    	getMainFrame().setTitle("hello world");
        jcv = new JChessView(this);
        // show adds the main default UI --> in this case JChessView
        show(jcv);        

        
    }
    

    /**
     * This method is to initialize the specified window by injecting resources.
     * Windows shown in our application come fully initialized from the GUI
     * builder, so this additional configuration is not needed.
     */
    @Override protected void configureWindow(java.awt.Window root) {
    }

    /**
     * A convenient static getter for the application instance.
     * @return the instance of JChessApp
     */
    public static JChessApp getApplication() {
        return Application.getInstance(JChessApp.class);
    }

    /**
     * Main method launching the application.
     * @param args x
     */
    public static void main(String[] args) {
        launch(JChessApp.class, args);
    }

	public static JChessView getJcv() {
		return jcv;
		
	}
}
