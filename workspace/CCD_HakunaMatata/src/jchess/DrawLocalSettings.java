/*
# Three Person Chess
# ------------------
#
# developed by: HakunaMatata
# workers: Anne Reich, Eric Schubert, Chris Taggeselle
*/
package jchess;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.BadLocationException;


/**
 * create setting-gui.
 * @author Anne Reich
 *
 */
@SuppressWarnings("serial")
public class DrawLocalSettings extends JPanel implements ActionListener, TextListener {

    private JDialog parent; //needet to close newGame window
    private JTextField firstName; //editable field 4 nickname
    private JTextField secondName; //editable field 4 nickname
    private JTextField thirdName; //editable field 4 nickname
    private JLabel firstNameLab;
    private JLabel secondNameLab;
    private JLabel thirdNameLab;
    private JLabel firstNameColor;
    private JLabel secondNameColor;
    private JLabel thirdNameColor;
    private GridBagLayout gbl;
    private GridBagConstraints gbc;
    private JButton okButton;     
        
    /** 
     * Method witch is checking correction of edit tables.
     * @param e Object where is saving this what contents edit tables
    */
	public void textValueChanged(TextEvent e) {
        final Object target = e.getSource();
        if (target == this.firstName || target == this.secondName 
        		  || target == this.thirdName) 
        {
            JTextField temp = new JTextField();
            if (target == this.firstName) {
                temp = this.firstName;
            }
            else if (target == this.secondName) {
                temp = this.secondName;
            }
            else if (target == this.thirdName) {
                temp = this.thirdName;
            }

            final int len = temp.getText().length();
            if (len > 8) {
                try {
                    temp.setText(temp.getText(0, 7));
                }
                catch (BadLocationException exc) {
                    System.out.println("Something wrong in editables: \n" + exc);
                }
            }
        }
    }

    /** 
     * Method responsible for changing the options which can make a player
     * when he want to start new local game.
     * @param e where is saving data of performed action
     */
	@SuppressWarnings("static-access")
	public void actionPerformed(ActionEvent e) {
        final Object target = e.getSource();
        if (target == this.okButton) {  
            if (this.firstName.getText().length() > 9) {
            	//make names short to 10 digits
                this.firstName.setText(this.trimString(firstName, 9));
            }
            if (this.secondName.getText().length() > 9) {
            	//make names short to 10 digits
                this.secondName.setText(this.trimString(secondName, 9));
            }
            if (this.thirdName.getText().length() > 9) {
            	//make names short to 10 digits
                this.thirdName.setText(this.trimString(thirdName, 9));
            }
            if (this.firstName.getText().length() == 0 || this.secondName.getText().length() == 0 
            		  || this.thirdName.getText().length() == 0) 
            {
                JOptionPane.showMessageDialog(this, Settings.lang("fill_names"));
                return;
            }
            
            JChessApp.getJcv().getGame().getSettings().getPlayerByID(1).setName(Settings.lang("white") + ": " + firstName.getText());
            JChessApp.getJcv().getGame().getSettings().getPlayerByID(2).setName(Settings.lang("green") + ": " + secondName.getText());
            JChessApp.getJcv().getGame().getSettings().getPlayerByID(3).setName(Settings.lang("black") + ": " + thirdName.getText());
            JChessApp.getJcv().getGame().newGame();
            JChessApp.getJcv().getGame().setRepaintChessboard(true);
            this.parent.setVisible(false); //hide parent

        }

    }

	/**
	 * Create Game Setting GUI.
	 * @param parent JDialog
	 */
    DrawLocalSettings(JDialog parent) {
        super();
        
        this.parent = parent;
        this.gbl = new GridBagLayout();
        this.gbc = new GridBagConstraints();
        this.okButton = new JButton(Settings.lang("ok"));

        this.firstName = new JTextField("", 10);
        this.firstName.setSize(new Dimension(200, 50));
        this.secondName = new JTextField("", 10);
        this.secondName.setSize(new Dimension(200, 50));
        this.thirdName = new JTextField("", 10);
        this.thirdName.setSize(new Dimension(200, 50));
        this.firstNameLab = new JLabel(Settings.lang("first_player_name") + ": ");
        this.secondNameLab = new JLabel(Settings.lang("second_player_name") + ": ");
        this.thirdNameLab = new JLabel(Settings.lang("third_player_name") + ": ");
        this.firstNameColor = new JLabel(Settings.lang("is_white"));
        this.secondNameColor = new JLabel(Settings.lang("is_green"));
        this.thirdNameColor = new JLabel(Settings.lang("is_black"));

        this.setLayout(gbl);
        this.okButton.addActionListener(this);        

        this.gbc.insets = new Insets(3, 3, 3, 3);
        
        //1st player
        this.gbc.gridx = 0;
        this.gbc.gridy = 0;
        this.gbl.setConstraints(firstNameLab, gbc);
        this.add(firstNameLab);
        this.gbc.gridy = 1;
        this.gbl.setConstraints(firstName, gbc);
        this.add(firstName);
        this.gbc.gridx = 1;
        this.gbc.gridy = 1;
        this.gbl.setConstraints(firstNameColor, gbc);
        this.add(firstNameColor);
       
        //2nd player
        this.gbc.gridx = 0;
        this.gbc.gridy = 2;
        this.gbl.setConstraints(secondNameLab, gbc);
        this.add(secondNameLab);
        this.gbc.gridy = 3;
        this.gbl.setConstraints(secondName, gbc);
        this.add(secondName);
        this.gbc.gridx = 1;
        this.gbc.gridy = 3;
        this.gbl.setConstraints(secondNameColor, gbc);
        this.add(secondNameColor);
        
        //3rd player
        this.gbc.gridx = 0;
        this.gbc.gridy = 4;
        this.gbl.setConstraints(thirdNameLab, gbc);
        this.add(thirdNameLab);
        this.gbc.gridy = 5;
        this.gbl.setConstraints(thirdName, gbc);
        this.add(thirdName);
        this.gbc.gridx = 1;
        this.gbc.gridy = 5;
        this.gbl.setConstraints(thirdNameColor, gbc);
        this.add(thirdNameColor);
        
        //start-button
        this.gbc.insets = new Insets(15, 0, 0, 0);
        this.gbc.gridx = 0;
        this.gbc.gridy = 6;
        this.gbc.gridwidth = 0;
        this.gbl.setConstraints(okButton, gbc);
        this.add(okButton);
    }

    /**
     * Method responsible for triming white symbols from strings.
     * @param txt Where is capt value to equal
     * @param length How long is the string
     * @return result trimmed String
     */
    public String trimString(JTextField txt, int length) {
        String result = "";
        try {
            result = txt.getText(0, length);
        }
        catch (BadLocationException exc) {
            System.out.println("Something wrong in editables: \n" + exc);
        }
        return result;
    }
}
