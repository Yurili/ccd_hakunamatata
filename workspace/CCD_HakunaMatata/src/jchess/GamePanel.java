/*
# Three Person Chess
# ------------------
#
# developed by: HakunaMatata
# workers: Anne Reich, Eric Schubert, Chris Taggeselle
*/

package jchess;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import jchess.gameLogic.ChessVector;
import jchess.gameLogic.Chessboard;
import jchess.gameLogic.GeometricOperations;
import jchess.gameLogic.Hexagon;
import jchess.gameLogic.Movement;

/**
 * Class responsible for the starts of new games, loading games, saving it, and
 * for ending it. This class is also responsible for appoing player with have a
 * move at the moment
 * 
 * @author Chris Taggeselle, Eric Schubert, Anne Reich
 */
@SuppressWarnings("serial")
public class GamePanel extends JPanel implements MouseListener {

	private Settings settings;
	private Chessboard chessboard;

	private boolean repaintChessboard;
	private Dimension sizeOfChessboard;
	// new Movement object
	private Movement movementObject;
	
	private int mittelpunktX;
	private int mittelpunktY;

	private int umkreisRadius;
	private int inKreisRadius;

	private float vecDiagonalX;
	private float vecDiagonalY;
	private float vecHorizontalX;
	private float vecHorizontalY;


	public static final int BOARD_MAX_HEXAGON = 15;

	/**
	 * Contains data structure of chessboard, mouseinteraction
	 * Listener, methods to draw pieces and hexagons and the 
	 * current settings.
	 * 
	 * @param x width of JPanel
	 * @param y height of JPanel
	 */
	public GamePanel(int x, int y) {

		this.sizeOfChessboard = new Dimension(x, y);
		this.computeGeometricProperties(sizeOfChessboard);
		this.movementObject = new Movement();
		this.chessboard = new Chessboard();
		settings = Settings.getInstance();

		this.setLayout(null);
		this.setBackground(Color.WHITE);
		this.setVisible(true);
		this.setLocation(new Point(0, 0));
		this.addMouseListener(this);
		this.setDoubleBuffered(true);
		this.setVisible(true);

		repaintChessboard = false;

	}
	
	/**
	 * Computes the center point of the JPanel, the geometric
	 * parameter of the hexagons including its polygons, and the vectors, needed
	 * to place the hexagons on the current graphicsItem.
	 * 
	 * @param currentDimension Dimension of the JPanel
	 */
	private void computeGeometricProperties(Dimension currentDimension) {

		setMittelpunktX((int) currentDimension.getWidth() / 2);
		setMittelpunktY((int) currentDimension.getHeight() / 2);

		umkreisRadius = (int) (currentDimension.getHeight())
				/ (BOARD_MAX_HEXAGON * 2);
		inKreisRadius = (int) (umkreisRadius * (Math.sqrt(3) / 2));

		vecDiagonalX = (float) (umkreisRadius * Math.cos(GeometricOperations.getInstance().getRadiansByAngle(30)));
		vecDiagonalY = (float) (2 * inKreisRadius * Math.sin(GeometricOperations.getInstance().getRadiansByAngle(60)));

		vecHorizontalX = (float) (2 * inKreisRadius * Math.cos(GeometricOperations.getInstance().getRadiansByAngle(0)));
		vecHorizontalY = 0;

		// um den faktor 1.2 --> erhoeht Uebersicht des Wabenmusters
		vecDiagonalX = (float) (vecDiagonalX * 1.2);
		vecDiagonalY = (float) (vecDiagonalY * 1.2);
		vecHorizontalX = (float) (vecHorizontalX * 1.2);
	}

	/**
	 * Repaint Manager calls method.
	 * 
	 * @param g is the current graphics item for the JPanel
	 */
	@Override
	public void paintComponent(Graphics g) {

		final Graphics2D g2d = (Graphics2D) this.getGraphics();
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		g2d.setColor(Color.WHITE);
		g2d.fillRect(0, 0, (int) sizeOfChessboard.getWidth(), (int) sizeOfChessboard.getHeight());

		if (repaintChessboard) {

			g2d.setColor(Color.WHITE);
			g2d.fillRect(0, 0, 200, 50);
			g2d.setColor(Color.BLACK);
			final String playerString = jchess.Settings.getActivePlayer().getName() + " " + jchess.Settings.getActivePlayer().getStatus().toString();
			//g2d.drawString(jchess.Settings.getActivePlayer().getName(), 20, 20);
			g2d.drawString(playerString, 40, 40);

			// TODO: Auslagern -> evtl eigene Klasse fuer geometrische
			// Operationen

			for (Hexagon elem : chessboard.getListHexagons()) {
				final float xNeu = getMittelpunktX() + elem.getPozX() * vecHorizontalX + elem.getPozY() * vecDiagonalX;
				final float yNeu = getMittelpunktY() + elem.getPozX() * vecHorizontalY + elem.getPozY() * vecDiagonalY;
				final Polygon tempPolygon = GeometricOperations.getInstance().getPolygonShapeByPointAndVertices(xNeu, yNeu, umkreisRadius, 6);
				elem.setGeometricHexagon(tempPolygon);
				g2d.drawPolygon(elem.getGeometricHexagon());

				if ((elem.getPozX() + 9) % 3 == (elem.getPozY() + 9) % 3) {
					drawHexagon(elem.getGeometricHexagon(), Color.WHITE);
				} 
				else if ((elem.getPozX() + 9) % 3 == (elem.getPozY() + 10) % 3) {
					drawHexagon(elem.getGeometricHexagon(), Color.BLACK);
				} 
				else {
					drawHexagon(elem.getGeometricHexagon(), Color.GREEN);
				}
			}

			for (Hexagon elem : chessboard.getListHexagons()) {

				if (elem.getPiece() != null) {
					final float xNeu = getMittelpunktX() + elem.getPozX() * vecHorizontalX + elem.getPozY() * vecDiagonalX;
					final float yNeu = getMittelpunktY() + elem.getPozX() * vecHorizontalY + elem.getPozY() * vecDiagonalY;

					final Image img = GeometricOperations.getInstance().scaleImage(elem.getPiece().getOrgImage(), 
							  (int) (inKreisRadius * 1.6), (int) (inKreisRadius * 1.6));

					final int x = (int) (xNeu - img.getWidth(null) * 0.5);
					final int y = (int) (yNeu - img.getHeight(null) * 0.5);
					g2d.drawImage(img, x, y, null);
				}
			}
		}
	}

	/**
	 * Fills an hexagon in the chessboard by the given color .
	 * 
	 * @param poly geometric position on the JPanel
	 * @param color color to fill
	 */
	public void drawHexagon(Polygon poly, Color color) {

		final Graphics2D g2d = (Graphics2D) this.getGraphics();
		final float alpha = 0.3f;
		final int type = AlphaComposite.SRC_OVER;
		final AlphaComposite composite = AlphaComposite.getInstance(type, alpha);
		g2d.setComposite(composite);
		g2d.setPaint(color);
		g2d.fillPolygon(poly);
	}

	/**
	 * Method to Start new game and there 
	 * it set new pieces for every player on the chessboard.
	 */
	public void newGame() {
		chessboard.setPieces(Settings.getPlayerByID(1), Settings.getPlayerByID(2), Settings.getPlayerByID(3));
		movementObject.updateAllMoves(chessboard);
	}

	/**
	 * Method to end game. Don't work yet.
	 * 
	 * @param message what to show player(s) at end of the game (for example "draw", "black wins" etc.)
	 */
	public void endGame(String message) {
		System.out.println(message);
		JOptionPane.showMessageDialog(null, message);
	}

	/**
	 * Mouser Interaction event enables the user an interaction on the chessboard.
	 * 
	 * @param event mouse event
	 * */
	public void mousePressed(MouseEvent event) {
		if (event.getButton() == MouseEvent.BUTTON3) { // right button
			repaintChessboard = true;
			this.paint(this.getGraphics());
		} 
		else if (event.getButton() == MouseEvent.BUTTON1) {
			try {
				final Hexagon hexagonOnChessboard = chessboard.getHexagonByPixel(event.getX(), event.getY());
				// Nur reagieren, wenn Hexagon angeklickt wurde.
				if (hexagonOnChessboard != null) {
					if (hexagonOnChessboard.isElementOfVectorSet(chessboard.getTargetHexagons())) {
						// Movement
						final Hexagon target = hexagonOnChessboard;
						final Hexagon source = chessboard.getSelectedHexagon();
						target.setPiece(source.getPiece());
						source.deletePiece();
						chessboard.deleteSelectedHexagon();
						chessboard.deleteTargetHexagons();
						movementObject.updateAllMoves(chessboard);
						Settings.switchActivePlayer();
						// chessboard.paint(chessboard.getGraphics());
						paint(this.getGraphics());
						// return;
					} 
					else if (hexagonOnChessboard.getPiece() == null) {
						// Abwaehlen von hervorgehobenen Hexagonen
						if (chessboard.getSelectedHexagon() != null) {
							chessboard.deleteSelectedHexagon();
							chessboard.deleteTargetHexagons();
							// chessboard.paint(chessboard.getGraphics());
							paint(this.getGraphics());
						}
					} 
					else if (hexagonOnChessboard.getPiece() != null) {
						if (hexagonOnChessboard == chessboard.getSelectedHexagon()) {
							chessboard.deleteSelectedHexagon();
							chessboard.deleteTargetHexagons();
							// chessboard.paint(chessboard.getGraphics());
							paint(this.getGraphics());
						} 
						else {
							// chessboard.paintAll(this.getGraphics());
							// chessboard.paint(chessboard.getGraphics());
							paint(this.getGraphics());
							// 1. query - active player
							if (hexagonOnChessboard.getPiece().getPlayer() == Settings.getActivePlayer()) {
								chessboard.setSelectedHexagon(hexagonOnChessboard);
								chessboard.setTargetHexagons(hexagonOnChessboard.getPiece().getAllMoves());
								// chessboard.drawHexagon(hexagonOnChessboard.getGeometricHexagon(),
								// Color.red);
								this.drawHexagon(hexagonOnChessboard
										  .getGeometricHexagon(), Color.red);
								for (ChessVector vec : hexagonOnChessboard.getPiece().getAllMoves()) {
									// chessboard.drawHexagon(chessboard.getHexagon(vec.getX(),
									// vec.getY()).getGeometricHexagon(),
									// Color.red);
									this.drawHexagon(
											  chessboard.getHexagon(vec.getX(), vec.getY()).getGeometricHexagon(), Color.red);
									// TODO: Do Draw Pieces
								}
							}
						}
					}
				}
			} 
			catch (NullPointerException exc) {
				System.err.println(exc.getMessage());
				System.out.println("Null Pointer in Game - MousePressed");
				return;
			}
		}
	}

	/**
	 * Interface MouseListener.
	 * @param arg0 
	 */
	public void mouseReleased(MouseEvent arg0) {
	}

	/**
	 * Interface MouseListener.
	 * @param arg0 
	 */
	public void mouseEntered(MouseEvent arg0) {
	}

	/**
	 * Interface MouseListener.
	 * @param arg0 
	 */
	public void mouseExited(MouseEvent arg0) {
	}

	public Settings getSettings() {
		return settings;
	}

	public Chessboard getChessboard() {
		return chessboard;
	}

	/**
	 * Interface MouseEvent.
	 * @param e
	 */
	public void mouseClicked(MouseEvent e) {
	}

	public int getMittelpunktX() {
		return mittelpunktX;
	}

	private void setMittelpunktX(int mittelpunktX) {
		this.mittelpunktX = mittelpunktX;
	}

	public int getMittelpunktY() {
		return mittelpunktY;
	}

	private void setMittelpunktY(int mittelpunktY) {
		this.mittelpunktY = mittelpunktY;
	}

	public boolean isRepaintChessboard() {
		return repaintChessboard;
	}

	public void setRepaintChessboard(boolean repaintChessboard) {
		this.repaintChessboard = repaintChessboard;
	}
}

