/*
# Three Person Chess
# ------------------
#
# developed by: HakunaMatata
# workers: Anne Reich, Eric Schubert, Chris Taggeselle
*/
package jchess;

import javax.swing.JDialog;
import javax.swing.WindowConstants;

/**
 * Dialog to set the settings for a new game.
 * @author Anne Reich, Eric Schubert, Chris Taggeselle
 */
@SuppressWarnings("serial")
public class NewGameWindow extends JDialog {

    /** 
     * Creates new form NewGameWindow with an default size.
     * */
    public NewGameWindow() {
        initComponents();

        this.setSize(400, 700);
        this.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        this.jTabbedPane1.addTab(Settings.lang("local_game"), new DrawLocalSettings(this));
    }

    /**
     * Initialized the layout of the Dialog.
     */
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setAlwaysOnTop(true);
        setName("Form"); // NOI18N

        jTabbedPane1.setName("jTabbedPane1"); // NOI18N

        final javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 376, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 268, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    } // </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

			public void run() {
                new NewGameWindow().setVisible(true);
            }
        });
    }

    
    
    private javax.swing.JTabbedPane jTabbedPane1;
}
