/*
# Three Person Chess
# ------------------
#
# developed by: HakunaMatata
# workers: Anne Reich, Eric Schubert, Chris Taggeselle
*/

package jchess;

import java.io.Serializable;

/**
 * Class representing the player in the game.
 * @author HakunaMatata
 */
@SuppressWarnings("serial")
public class Player implements Serializable {

    private String name;
    private static int numberOfPlayer = 0;
    private int id;
    private STATUS status;
    
    /**
     * represents the status, a player can have.
     */
    public enum STATUS {
    	idle, checked, checkmated
    }
    
    /**
     * Constructor for a new player.
     * default STATUS is idle.
     * Increases automatically the number of player.
     * Every new player has its own id for identification.
     * @param name name of the new player
     */
    public Player(String name) {
    	this.name = name;
    	this.status = STATUS.idle;
    	
    	numberOfPlayer++;
    	id = numberOfPlayer;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
    
    public int getId() {
    	return this.id;
    }

	public STATUS getStatus() {
		return status;
	}

	public void setStatus(STATUS status) {
		this.status = status;
	}
}

