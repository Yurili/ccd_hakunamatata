/*
# Three Person Chess
# ------------------
#
# developed by: HakunaMatata
# workers: Anne Reich, Eric Schubert, Chris Taggeselle
*/

package jchess.pieces;


import java.awt.Image;
import java.util.HashSet;
import java.util.Set;

import jchess.Player;
import jchess.gameLogic.ChessVector;

/**Class to represent all chess pieces of any player.
 * 
 * @author Eric Schubert
 *
 */
public abstract class Piece {
    
	private Player player;
    private Image orgImage;
    
    private Set<ChessVector> allMoves;
    
    /** Constructor for the pieces super class. 
     * 
     * @param originalImage the image representing the piece on the chessboard
     * @param player the player, the piece belongs to
     */
    public Piece(Image originalImage, Player player) {
    	this.orgImage = originalImage;
    	this.player = player;
    	this.allMoves = new HashSet<ChessVector>();
    }

	public Player getPlayer() {
		return player;
	}

	public Image getOrgImage() {
		return orgImage;
	}

	public Set<ChessVector> getAllMoves() {
		return allMoves;
	}

	public void setAllMoves(Set<ChessVector> allMoves) {
		this.allMoves = allMoves;
	}  
}
