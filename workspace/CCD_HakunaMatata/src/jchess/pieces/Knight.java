/*
# Three Person Chess
# ------------------
#
# developed by: HakunaMatata
# workers: Anne Reich, Eric Schubert, Chris Taggeselle
*/

package jchess.pieces;

import java.awt.Image;

import jchess.Player;

/**
 * 
 * @author yurili
 *
 */
public class Knight extends Piece {   
	
	/**
	 * 
	 * @param originalImage x
	 * @param player x
	 */
	public Knight(Image originalImage, Player player) {
    	super(originalImage, player);
    }
}
