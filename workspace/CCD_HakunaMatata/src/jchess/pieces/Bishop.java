/*
# Three Person Chess
# ------------------
#
# developed by: HakunaMatata
# workers: Anne Reich, Eric Schubert, Chris Taggeselle
*/

package jchess.pieces;

import java.awt.Image;

import jchess.Player;

/**
 * 
 * @author yurili
 *
 */
public class Bishop extends Piece {   

	/**
	 * 
	 * @param originalImage xxx
	 * @param player xxx
	 */
	public Bishop(Image originalImage, Player player) {
    	super(originalImage, player);
    }
}
