/*
# Three Person Chess
# ------------------
#
# developed by: HakunaMatata
# workers: Anne Reich, Eric Schubert, Chris Taggeselle
*/
package jchess;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JDialog;
import javax.swing.JFrame;

import org.jdesktop.application.Action;
import org.jdesktop.application.FrameView;
import org.jdesktop.application.ResourceMap;
import org.jdesktop.application.SingleFrameApplication;



/**
 * JChess View contains all necessary object for user interaction.     
 * It is the  application's main frame.
 * @author Anne Reich, Chris Taggeselle
 */
public class JChessView extends FrameView implements ActionListener {
	
    /**
     * @param event xxx
     */
	public void actionPerformed(ActionEvent event) {
        final Object target = event.getSource();
        if (target == newGameItem) {
            this.newGameFrame = new NewGameWindow();
            JChessApp.getApplication().show(this.newGameFrame);
        }
	}   

	/**
	 * Initialize FrameView on JChessApp.
	 * @param app SingleFrameApplication for FrameView
	 */
    public JChessView(SingleFrameApplication app) {
        super(app);
        
        initComponents();
        // status bar initialization - message timeout, idle icon and busy animation, etc
        final ResourceMap resourceMap = getResourceMap();
        resourceMap.getInteger("StatusBar.messageTimeout");
    }

    /**
     * Displays information about the software.
     */
    @Action
    public void showAboutBox() {
        if (aboutBox == null) {
            final JFrame mainFrame = JChessApp.getApplication().getMainFrame();
            aboutBox = new JChessAboutBox(mainFrame);
            aboutBox.setLocationRelativeTo(mainFrame);
        }
        JChessApp.getApplication().show(aboutBox);
    }
 


    /**     
	* Initializes the components of the mainframe, for example the dimension, menu Bar Items and so on.
	*/
    private void initComponents() {

    	fixDimension =  new  Dimension(1000, 800);
    	
        newGame = new GamePanel(fixDimension.width, fixDimension.height);
        getFrame().setMinimumSize(fixDimension);
        getFrame().setResizable(false);
     
        menuBar = new javax.swing.JMenuBar();
        final javax.swing.JMenu fileMenu = new javax.swing.JMenu();
        newGameItem = new javax.swing.JMenuItem();
        final javax.swing.JMenuItem exitMenuItem = new javax.swing.JMenuItem();
        
        final javax.swing.JMenu helpMenu = new javax.swing.JMenu();
        final javax.swing.JMenuItem aboutMenuItem = new javax.swing.JMenuItem();
        
        final javax.swing.JSeparator statusPanelSeparator = new javax.swing.JSeparator();

        menuBar.setName("menuBar"); // NOI18N

        final org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application
        		  .getInstance(jchess.JChessApp.class).getContext().getResourceMap(JChessView.class);
        fileMenu.setText(resourceMap.getString("fileMenu.text")); // NOI18N
        fileMenu.setName("fileMenu"); // NOI18N

        newGameItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        newGameItem.setText(resourceMap.getString("newGameItem.text")); // NOI18N
        newGameItem.setName("newGameItem"); // NOI18N
        fileMenu.add(newGameItem);
        newGameItem.addActionListener(this);

        final javax.swing.ActionMap actionMap = org.jdesktop.application.Application
        		  .getInstance(jchess.JChessApp.class).getContext().getActionMap(JChessView.class, this);
        exitMenuItem.setAction(actionMap.get("quit")); // NOI18N
        exitMenuItem.setName("exitMenuItem"); // NOI18N
        fileMenu.add(exitMenuItem);

        menuBar.add(fileMenu);

        helpMenu.setText(resourceMap.getString("helpMenu.text")); // NOI18N
        helpMenu.setName("helpMenu"); // NOI18N

        aboutMenuItem.setAction(actionMap.get("showAboutBox")); // NOI18N
        aboutMenuItem.setName("aboutMenuItem"); // NOI18N
        helpMenu.add(aboutMenuItem);

        menuBar.add(helpMenu);

   //     statusPanel.setName("statusPanel"); // NOI18N

        statusPanelSeparator.setName("statusPanelSeparator"); // NOI18N
        
        
        
        setComponent(newGame);
        setMenuBar(menuBar);

    } // </editor-fold>//GEN-END:initComponents


    private Dimension fixDimension;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem newGameItem;
    private jchess.GamePanel newGame;


    private JDialog aboutBox;
    private JDialog newGameFrame;
	
	public GamePanel getGame() {
		return this.newGame;
	}


	public JDialog getNewGameFrame() {
		return newGameFrame;
	}

	public void setNewGameFrame(JDialog newGameFrame) {
		this.newGameFrame = newGameFrame;
	}
    
}
