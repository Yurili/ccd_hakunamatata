/*
# Three Person Chess
# ------------------
#
# developed by: HakunaMatata
# workers: Anne Reich, Eric Schubert, Chris Taggeselle
*/

package jchess.test;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;

import jchess.Player;
import jchess.Settings;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Tests, whether Settings is a singleton and the logic of player will works good..
 * @author Chris Taggeselle
 *
 */
public class TestSettings {

	private static Settings settings;

	
	/**
	 * Initialized member.
	 * @throws java.lang.Exception x
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		settings = Settings.getInstance();
	
	}
	
	/**
	 * Tests, whether at least one instance will be created.
	 */
	@Test
	public void testGetInstance() {
		assertNotNull(settings);
	}

	/**
	 *  Tests, whether every player in the list is available.
	 */
	@Test
	public void testGetPlayerById() {

		for (int i = 1; i <= Settings.getListPlayer().size(); i++) {
			assertNotNull(Settings.getPlayerByID(1));
			assertNotNull(Settings.getPlayerByID(2));
			assertNotNull(Settings.getPlayerByID(3));
		}
	}

	/** 
	 * Tests, if the method switchting the player works correctly.
	 */
	@Test
	public void testSwitchActivePlayer() {

		// first Test: if switching player really switch the active player
		final Player activePlayerBefore = Settings.getActivePlayer();
		Settings.switchActivePlayer();
		final Player activePlayerAfter = Settings.getActivePlayer();
		assertNotSame(activePlayerBefore, activePlayerAfter);

		// second test: order of switching must be correct
		for (Player activePlayer : Settings.getListPlayer()) {
			Settings.setActivePlayer(activePlayer);
			Settings.switchActivePlayer();
			final int delta = 0;
			if (activePlayer.getId() < Settings.getListPlayer().size()) {
				assertEquals("Test Settings: Switch to next player", activePlayer.getId() + 1, Settings.getActivePlayer().getId(), delta);
			} 
			else if (activePlayer.getId() == Settings.getListPlayer().size()) {
				assertEquals("Test Settings: Switch to 1 player", 1, Settings.getActivePlayer().getId(), delta);
			}
		}
	}
}
