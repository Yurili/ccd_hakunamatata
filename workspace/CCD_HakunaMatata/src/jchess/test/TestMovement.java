/*
# Three Person Chess
# ------------------
#
# developed by: HakunaMatata
# workers: Anne Reich, Eric Schubert, Chris Taggeselle
*/

package jchess.test;

import static org.junit.Assert.assertTrue;
import jchess.gameLogic.ChessVector;
import jchess.gameLogic.Chessboard;
import jchess.gameLogic.Hexagon;
import jchess.gameLogic.Movement;
import jchess.pieces.Bishop;
import jchess.pieces.Pawn;
import jchess.pieces.Rook;
import jchess.ImageLoader;
import jchess.Settings;

import org.junit.Test;

/**
 * 
 * @author Eric Schubert
 *
 */
public class TestMovement {
	
	private Chessboard board;
	private Movement moveObeject;
	
	/**
	 * 
	 */
	public TestMovement() {
		this.board = new Chessboard();
		this.moveObeject = new Movement();
		Settings.getInstance();
		
		board.getHexagon(0, 0).setPiece(new Rook(ImageLoader.loadImage("Rook-W.png"), Settings.getPlayerByID(1)));
		board.getHexagon(-5, 0).setPiece(new Pawn(ImageLoader.loadImage("Pawn-G.png"), Settings.getPlayerByID(2)));
		board.getHexagon(-2, 4).setPiece(new Bishop(ImageLoader.loadImage("Bishop-B.png"), Settings.getPlayerByID(3)));
	}
	
	/**
	 * test for Movement updateMoves().
	 */
	@Test
	public void updateMoves() {
		
		moveObeject.updateAllMoves(board);
		
		final ChessVector posRook1 = new ChessVector(-5, 0);
		final ChessVector posRook2 = new ChessVector(3, -3);
		
		final ChessVector posBishop = new ChessVector(0, 0);
		
		final ChessVector posPawn = new ChessVector(-5, -1);
		
		final boolean condRook1 = containsTargetVector(board.getHexagon(0, 0), posRook1);
		final boolean condRook2 = containsTargetVector(board.getHexagon(0, 0), posRook2);
		final boolean condBishop = containsTargetVector(board.getHexagon(-2, 4), posBishop);
		final boolean condPawn = containsTargetVector(board.getHexagon(-5, 0), posPawn);
		
		assertTrue("Rook position 1:", condRook1);
		assertTrue("Rook position 2:", condRook2);
		assertTrue("Bishop position 3:", condBishop);
		assertTrue("Pawn position 4:", condPawn);
		
	}
	
	/** Checks, if the target ChessVector is contained in the positions piece Set "allMoves".
	 * 
	 * @param position the hexagon of the piece
	 * @param target the target to be checked
	 * @return true, if target is reachable, otherwise false
	 */
	private boolean containsTargetVector(Hexagon position, ChessVector target) {
		
		boolean condition = false;
		
		for (ChessVector vec : position.getPiece().getAllMoves()) {
			if (vec.equals(target)) {
				condition = true;
			}
		}
		
		return condition;		
	}
}
