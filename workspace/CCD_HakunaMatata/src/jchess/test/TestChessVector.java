/*
# Three Person Chess
# ------------------
#
# developed by: HakunaMatata
# workers: Anne Reich, Eric Schubert, Chris Taggeselle
*/

package jchess.test;

import static org.junit.Assert.assertEquals;
import jchess.gameLogic.ChessVector;

import org.junit.Test;


/**Class for doing JUnit-Tests.
 * 
 * @author Eric Schubert 
 *
 */
public class TestChessVector {

	/**
	 * 
	 */
	public TestChessVector() {
		
	}
	
	/**
	 * test for ChessVector sumVector().
	 */
	@Test
	public void sumVector() {
		
		final ChessVector r1 = new ChessVector(2, -1);
		final ChessVector r2 = new ChessVector(5, 1);
		final ChessVector r3 = new ChessVector(-8, 0);
		
		final ChessVector a1 = new ChessVector(3, 4);
		final ChessVector a2 = new ChessVector(0, 0);
		final ChessVector a3 = new ChessVector(2, -8);
		
		final ChessVector b1 = new ChessVector(-1, -5);
		final ChessVector b2 = new ChessVector(5, 1);
		final ChessVector b3 = new ChessVector(-10, 8);
		
		assertEquals("Adding 2 Vectors", r1, ChessVector.sumOfVectors(a1, b1));
		assertEquals("Adding 2 Vectors", r2, ChessVector.sumOfVectors(a2, b2));
		assertEquals("Adding 2 Vectors", r3, ChessVector.sumOfVectors(a3, b3));
	}

	/**
	 * test for ChessVector add().
	 */
	@Test
	public void add() {
		
		final ChessVector n = new ChessVector(-6, -1);
		
		final ChessVector a = new ChessVector(6, -3);
		final ChessVector b = new ChessVector(10, 1);
		final ChessVector c = new ChessVector(0, 0);
		
		a.add(n);
		b.add(n);
		c.add(n);
		
		final ChessVector ra = new ChessVector(0, -4);
		final ChessVector rb = new ChessVector(4, 0);
		final ChessVector rc = new ChessVector(-6, -1);
		
		assertEquals("Vector adding 1: ", ra, a);
		assertEquals("Vector adding 2: ", rb, b);
		assertEquals("Vector adding 3: ", rc, c);
		
	}
	
	/**
	 * test for ChessVector times().
	 */
	@Test
	public void times() {
		
		final ChessVector a = new ChessVector(2, -3);
		final ChessVector b = new ChessVector(0, 0);
		
		final ChessVector r1 = new ChessVector(4, -6);
		final ChessVector r2 = new ChessVector(-6, 9);
		final ChessVector r3 = new ChessVector(0, 0);
		
		assertEquals("Scalar multiplication 1: ", r1, a.times(2));
		assertEquals("Scalar multiplication 2: ", r2, a.times(-3));
		assertEquals("Scalar multiplication 3: ", r3, b.times(66));
		assertEquals("Scalar multiplication 4: ", r3, a.times(0));
	}
	
	/**
	 * test for ChessVector isDiagonal().
	 */
	@Test
	public void isDiagonal() {
		final ChessVector a1 = new ChessVector(2, -1);
		final ChessVector a2 = new ChessVector(-1, -1);
		final ChessVector a3 = new ChessVector(0, 1);
		final ChessVector a4 = new ChessVector(2, -2);
		final ChessVector a5 = new ChessVector(2, -4);
		
		assertEquals("Control diagonality 1: ", true, a1.isDiagonal());
		assertEquals("Control diagonality 2: ", true, a2.isDiagonal());
		assertEquals("Control diagonality 3: ", false, a3.isDiagonal());
		assertEquals("Control diagonality 4: ", false, a4.isDiagonal());
		assertEquals("Control diagonality 5: ", true, a5.isDiagonal());
		
	}
	
	/**
	 * test for ChessVector equals().
	 */
	@Test
	public void equals() {
		final ChessVector a = new ChessVector(2, -1);
		final ChessVector b = new ChessVector(-1, -1);
		final ChessVector c = new ChessVector(0, 0);
		final ChessVector d = new ChessVector(2, -1);
		
		assertEquals("Control equality 1: ", true, a.equals(d));
		assertEquals("Control equality 2: ", false, a.equals(b));
		assertEquals("Control equality 3: ", false, c.equals(d));
		assertEquals("Control equality 4: ", true, b.equals(b));
	}
}
