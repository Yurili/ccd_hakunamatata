/*
# Three Person Chess
# ------------------
#
# developed by: HakunaMatata
# workers: Anne Reich, Eric Schubert, Chris Taggeselle
*/

package jchess.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.awt.Dimension;

import jchess.GamePanel;
import jchess.gameLogic.Chessboard;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * TestClass for testing of the functionality of TestGamePanel.
 * 
 * @author Chris Taggeselle
 *
 */
public class TestGamePanel {

	private static GamePanel testGamePanel;
	private static Dimension testDimension;
	private static Chessboard testChessboard;

	/**
	 * Initialized member.
	 * 
	 * @throws java.lang.Exception x
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		testDimension = new Dimension(100, 100);
		testGamePanel = new GamePanel(testDimension.width, testDimension.height);
		testChessboard = testGamePanel.getChessboard();
	}

	/**
	 * Tests, whether constructor can create an chessboard object.
	 */
	@Test
	public void testGamePanelConstructor() {
		// 1. GamePanel need chessboard
		assertNotNull(testGamePanel.getChessboard());
		// 2. GamePanel needs new center point immediately
		final int delta = 1;
		assertEquals("center point x", testGamePanel.getMittelpunktX(), (int) (testDimension.width * 0.5), delta);
		assertEquals("center point y", testGamePanel.getMittelpunktY(), (int) (testDimension.height * 0.5), delta);
	}

	/**
	 * Tests, whether the center point is computed correctly. This could happen
	 * for example after resize events.
	 * 
	 */
	@Test
	public void testComputeGeometricProperties() {
		final int delta = 1;
		// 2. compute new center point
		assertEquals("center point x", testGamePanel.getMittelpunktX(), (int) (testDimension.width * 0.5), delta);
		assertEquals("center point y", testGamePanel.getMittelpunktY(), (int) (testDimension.height * 0.5), delta);
	}

}
