/*
# Three Person Chess
# ------------------
#
# developed by: HakunaMatata
# workers: Anne Reich, Eric Schubert, Chris Taggeselle
*/

package jchess.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.awt.Image;
import java.awt.image.BufferedImage;

import jchess.gameLogic.GeometricOperations;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Tests, whether all geometric operations works correctly.
 * @author Chris Taggeselle
 *
 */
public class TestGeometricOperations {

	private static GeometricOperations testClassGeometricOperations;
	
	/**
	 * Initialized member.
	 * @throws java.lang.Exception x
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		testClassGeometricOperations = GeometricOperations.getInstance();
	
	}
	
	/**
	 * Test, whether the getRadiansByAngle method works correctly.
	 * ToDo: Insert random tasks.
	 */
	@Test
	public void testGetRadiansByAngle() {
				
		final double delta = 0.1; 		// as long as delta > expected - acutal --> = true
		assertEquals("expected", 0, testClassGeometricOperations.getRadiansByAngle(0), delta);
		assertEquals("expected", 1.57, testClassGeometricOperations.getRadiansByAngle(90), delta);
		assertEquals("expected", 3.14, testClassGeometricOperations.getRadiansByAngle(180), delta);
		assertEquals("expected", 6.28, testClassGeometricOperations.getRadiansByAngle(360), delta);
	}
	
	/**
	 * Tests, whether at least one instance will be created.
	 */
	@Test
	public void testGetInstance() {
		assertNotNull(GeometricOperations.getInstance());
	}
	
	/**
	 * Test, whether the scaling method works correctly.
	 */
	@Test
	public void testScaleImage() {
		
		final int widhtBefore  = 50;
		final int heigthBefore = 50;
		
		final int widthAfter = 200;
		final int heightAfter = 300;
		
		final BufferedImage bi = new BufferedImage(widhtBefore, heigthBefore, BufferedImage.TYPE_INT_ARGB);
		final Image testImage = testClassGeometricOperations.scaleImage(bi, widthAfter, heightAfter);
		
		final int delta = 1;
		assertEquals("image witdth ", widthAfter, testImage.getWidth(null), delta);
		assertEquals("image witdth ", heightAfter, testImage.getHeight(null), delta);
	}
}
